﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using Newtonsoft.Json;

namespace ClientTest
{
    public class PlayerScore
    {
        public int Id { get; set; }
        public String PlayerName { get; set; }
        public int score { get; set; }
    }

    class Program
    {
        static HttpClient client = new HttpClient();
        
        static async Task<PlayerScore> GetPlayerScoreAsync(string path)
        {
            PlayerScore playerscore = null;
            
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                playerscore = await response.Content.ReadAsAsync<PlayerScore>();
            }
            
            return playerscore;
        }

        static async Task CreatePlayerScoreAsync(PlayerScore playerscore)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync("api/PlayerScores", playerscore);
            response.EnsureSuccessStatusCode();
            if(response.IsSuccessStatusCode)
            {
                Console.WriteLine("Added PlayerScore");
            }
        }

        static async Task<List<PlayerScore>> GetPlayerScoreByName(string path)
        {
            List<PlayerScore> playerscore = new List<PlayerScore>();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                playerscore = await response.Content.ReadAsAsync<List<PlayerScore>>();
            }
            else
            {
                Console.WriteLine("Username could not be found please try again \n");
            }
            return playerscore;
        }

        //static async Task CreatePlayerScoreAsync(PlayerScore playerscore)
        //{
        //    HttpResponseMessage response = await client.PostAsJsonAsync("api/PlayerScores", playerscore);
        //    response.EnsureSuccessStatusCode();
        //
        //    var url = response.Headers.Location.AbsoluteUri;
        //}
        static async Task<PlayerScore> UpdatePlayerScoreAsync(PlayerScore playerscore)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync($"api/PlayerScores/62", playerscore);
            response.EnsureSuccessStatusCode();
            if(response.IsSuccessStatusCode)
            {
                Console.WriteLine("Added PlayerScore");
            }
            // Deserialize the updated product from the response body.
            playerscore = await response.Content.ReadAsAsync<PlayerScore>();
            return playerscore;
        }

        static async Task<HttpStatusCode> DeletePlayerScoreAsync(int id)
        {
            HttpResponseMessage response = await client.DeleteAsync($"api/PlayerScores/{id}");
            return response.StatusCode;
        }

        static void ShowPlayerScore(PlayerScore playerscore)
        {
            Console.WriteLine($"ID: {playerscore.Id}\tName: {playerscore.PlayerName}\tScore: {playerscore.score}");
        }

        static void Main()
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            client.BaseAddress = new Uri("http://localhost:57086/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                PlayerScore playerscore1 = new PlayerScore {Id = 62, PlayerName = "Gavino", score = 222 };

                await CreatePlayerScoreAsync(playerscore1);
                //Console.WriteLine($"Created Player");

                Console.WriteLine("Testing new test\n");
                List<PlayerScore> list = await GetPlayerScoreByName("api/PlayerScores/getUserList/G");
                list.ForEach(i => Console.Write("{0}\n", i.PlayerName + ": " + i.score));
                //get
                Console.WriteLine("0");
                await GetPlayerScoreAsync("api/PlayerScores/3");
                Console.WriteLine("1");
                ShowPlayerScore(playerscore1);
                Console.WriteLine("2");

                //Update
                Console.WriteLine("Updating playerscore");
                playerscore1.score = 80;
                await UpdatePlayerScoreAsync(playerscore1);

                //get the updated playerscore
                //playerscore1 = await GetPlayerScoreAsync(url.PathAndQuery);
                ShowPlayerScore(playerscore1);
                //delete
                var statuscode = await DeletePlayerScoreAsync(playerscore1.Id);
                Console.WriteLine($"Deleted (Http Status = {(int)statuscode})");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
    }
}
