﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UnityGameWebApi.Models
{
    public class PlayerScore
    {
        public int Id { get; set; }
        public String PlayerName { get; set; }
        public int score { get; set; }
    }
}