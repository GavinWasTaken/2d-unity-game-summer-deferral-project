﻿CREATE TABLE [dbo].[PlayerScores] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [PlayerName] NVARCHAR (MAX) NULL,
    [score]      INT            NOT NULL
);

