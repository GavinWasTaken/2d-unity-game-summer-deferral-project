﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using UnityGameWebApi.Models;

namespace Unity_Game_Web_API.Controllers
{
    [RoutePrefix("api/PlayerScores")]
    public class PlayerScoresController : ApiController
    {
        public class playerscoreCollection
        {
            public PlayerScore[] ps { get; set;}
        }


        private UnityGameWebApiContext db = new UnityGameWebApiContext();

        // GET: api/PlayerScores
        [Route("")]
        public IQueryable<PlayerScore> GetPlayerScores()
        {
            return db.PlayerScores;
        }

        // GET: api/PlayerScores/5

        [ResponseType(typeof(PlayerScore))]
        public async Task<IHttpActionResult> GetPlayerScore(int id)
        {
            PlayerScore playerScore = await db.PlayerScores.FindAsync(id);
            if (playerScore == null)
            {
                return NotFound();
            }

            return Ok(playerScore);
        }
        //[Route("getUserList/{username}")]
        //public IEnumerable<PlayerScore> GetPlayerScoreList(String username)
        //{
        //    var result = new playerscoreCollection();
        //    var dbPlayerScores = db.PlayerScores;
        //    //var psList = dbPlayerScores.Where(t => t.PlayerName == username).Select(x => new PlayerScore(){ PlayerName = x.PlayerName, score = x.score }).ToArray();
        //    var psList = dbPlayerScores.Where(t => t.PlayerName == username).ToArray();
        //    result.ps = psList;
        //    if (psList == null || !psList.Any())
        //    {
        //        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
        //    }
        //    return result;
        //}
        [Route("getUserList/{username}")]
        public playerscoreCollection GetPlayerScoreList(String username)
        {
            var result = new playerscoreCollection();
            var dbPlayerScores = db.PlayerScores;
            //var psList = dbPlayerScores.Where(t => t.PlayerName == username).Select(x => new PlayerScore(){ PlayerName = x.PlayerName, score = x.score }).ToArray();
            var psList = dbPlayerScores.Where(t => t.PlayerName == username).ToArray();
            result.ps = psList;
            if (psList == null || !psList.Any())
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return result;
        }

        // PUT: api/PlayerScores/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPlayerScore(int id, PlayerScore playerScore)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != playerScore.Id)
            {
                return BadRequest();
            }

            db.Entry(playerScore).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerScoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(playerScore);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PlayerScores
        [ResponseType(typeof(PlayerScore))]
        [Route("postPlayer/{playerScore}")]
        public async Task<IHttpActionResult> PostPlayerScore(PlayerScore playerScore)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(PlayerScoreExists(playerScore.Id))
            {
                db.PlayerScores.Add(playerScore);
                await db.SaveChangesAsync(); 
            }
            return Ok(playerScore);

            //return Ok(CreatedAtRoute("DefaultApi", new { id = playerScore.Id }, playerScore));

        }

        // DELETE: api/PlayerScores/5
        [ResponseType(typeof(PlayerScore))]
        public async Task<IHttpActionResult> DeletePlayerScore(int id)
        {
            PlayerScore playerScore = await db.PlayerScores.FindAsync(id);
            if (playerScore == null)
            {
                return NotFound();
            }

            db.PlayerScores.Remove(playerScore);
            await db.SaveChangesAsync();

            return Ok(playerScore);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PlayerScoreExists(int id)
        {
            return db.PlayerScores.Count(e => e.Id == id) > 0;
        }
    }
}