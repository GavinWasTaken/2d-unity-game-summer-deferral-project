﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UnityGameWebApi.Models;
namespace Unity_Game_Web_API.Controllers
{
    public class PlayerScoreController : ApiController
    {
        PlayerScore[] playerScores = new PlayerScore[]
        {
            new PlayerScore() {Id = 1, PlayerName = "Gavin", score = 10000 },
            new PlayerScore() {Id = 2, PlayerName = "Hazel", score = 5000 },
            new PlayerScore() {Id = 3, PlayerName = "Daniel", score = 1000 }
        };
        // GET: api/PlayerScore
        public IEnumerable<PlayerScore> Get()
        {
            return playerScores;
        }

        // GET: api/PlayerScore/5
        public IHttpActionResult Get(int id)
        {
            PlayerScore player = playerScores.FirstOrDefault<PlayerScore>(c => c.Id == id);

            if (player == null)
            {
                return NotFound();
            }
            return Ok(player);
        }

        
        // POST: api/PlayerScore
        public IEnumerable<PlayerScore> Post([FromBody]PlayerScore newPlayerScore)
        {
            List<PlayerScore> playerList = playerScores.ToList<PlayerScore>();
            //newPlayerScore.Id = playerList.Count;
            playerList.Add(newPlayerScore);
            playerScores = playerList.ToArray();

            return playerScores;
        }

        

        // PUT: api/PlayerScore/5
        public IEnumerable<PlayerScore> Put(int id, [FromBody]PlayerScore changedPlayer)
        {
            PlayerScore player = playerScores.FirstOrDefault<PlayerScore>(c => c.Id == id);
            if (player != null)
            {
                player.PlayerName = changedPlayer.PlayerName;
                player.score = changedPlayer.score;
            }

            return playerScores;
        }

        // DELETE: api/PlayerScore/5
        public IEnumerable<PlayerScore> Delete(int id)
        {
            playerScores = playerScores.Where<PlayerScore>(c => c.Id != id).ToArray<PlayerScore>();
            return playerScores;
        }
    }
}
