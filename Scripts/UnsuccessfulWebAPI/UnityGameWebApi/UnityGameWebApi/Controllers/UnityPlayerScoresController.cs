﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using UnityGameWebApi.Models;

namespace UnityGameWebApi.Controllers
{
    [RoutePrefix("api/UnityPlayerScores")]
    public class UnityPlayerScoresController : ApiController
    {
        
        private UnityGameWebApiContext db = new UnityGameWebApiContext();

        // GET: api/UnityPlayerScores
        public IQueryable<PlayerScore> GetPlayerScores()
        {
            return db.PlayerScores;
        }

        // GET: api/UnityPlayerScores/5
        [ResponseType(typeof(PlayerScore))]
        public async Task<IHttpActionResult> GetPlayerScore(int id)
        {
            PlayerScore playerScore = await db.PlayerScores.FindAsync(id);
            if (playerScore == null)
            {
                return NotFound();
            }

            return Ok(playerScore);
        }

        // PUT: api/UnityPlayerScores/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPlayerScore(int id, PlayerScore playerScore)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != playerScore.Id)
            {
                return BadRequest();
            }

            db.Entry(playerScore).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerScoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UnityPlayerScores
        [ResponseType(typeof(PlayerScore))]
        public async Task<IHttpActionResult> PostPlayerScore(PlayerScore playerScore)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PlayerScores.Add(playerScore);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = playerScore.Id }, playerScore);
        }
        //Another POST for posting user score for Unity
        [HttpPost]
        [AcceptVerbs("POST")]
        [Route("PostUnity")]
        public async void PostPlayerUnity(string PlayerName, int score)
        {
            PlayerScore unityPlayerScore = new PlayerScore { Id = 1, PlayerName = PlayerName, score = score };
            if (PlayerScoreExists(unityPlayerScore.Id))
            {
                db.PlayerScores.Add(unityPlayerScore);
                await db.SaveChangesAsync();
            }
        }

        public IEnumerable<PlayerScore> PostUnityPlayer(string PlayerName, int score)
        {
            PlayerScore unityPlayerScore = new PlayerScore { Id = 1, PlayerName = PlayerName, score = score };
            if (PlayerScoreExists(unityPlayerScore.Id))
            {
                db.PlayerScores.Add(unityPlayerScore);
                db.SaveChangesAsync();
            }

            yield return unityPlayerScore;
        }
        // DELETE: api/UnityPlayerScores/5
        [ResponseType(typeof(PlayerScore))]
        public async Task<IHttpActionResult> DeletePlayerScore(int id)
        {
            PlayerScore playerScore = await db.PlayerScores.FindAsync(id);
            if (playerScore == null)
            {
                return NotFound();
            }

            db.PlayerScores.Remove(playerScore);
            await db.SaveChangesAsync();

            return Ok(playerScore);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PlayerScoreExists(int id)
        {
            return db.PlayerScores.Count(e => e.Id == id) > 0;
        }
    }
}