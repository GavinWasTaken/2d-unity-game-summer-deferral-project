namespace UnityGameWebApi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using UnityGameWebApi.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<UnityGameWebApi.Models.UnityGameWebApiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(UnityGameWebApi.Models.UnityGameWebApiContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.PlayerScores.AddOrUpdate(new PlayerScore[] {
              new PlayerScore {Id = 1, PlayerName = "Adam", score = 10000 },
              new PlayerScore {Id = 2, PlayerName = "Ben" , score = 5000},
              new PlayerScore {Id = 3, PlayerName = "Chris" , score = 1000}
            });

        }
    }
}
