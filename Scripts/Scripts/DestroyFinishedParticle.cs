﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is to be used with the partciles systems throughout the game to let them play only once and then to 
// be destroyed
public class DestroyFinishedParticle : MonoBehaviour {
    // set the instance of the particle system
    private ParticleSystem theParticleSystem;
	// Use this for initialization
	void Start () {
        // the instance to be equal to the particle system attached to the game object with this script on it
        theParticleSystem = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        // if the particle system is playing let it play
		if(theParticleSystem.isPlaying)
        {
            return;
        }
        // otherwise the object is to be removed from the scene completely by destroying it
        Destroy(gameObject);
	}
}
//[GAVIN END]