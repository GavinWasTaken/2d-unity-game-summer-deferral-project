﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this script is used with the basic enemy type of the skeleton enemy
// along with its behavior and actions within the game

public class BasicSkeletonController : MonoBehaviour {

    //swetts a float used for the speed in which the gameobject will move
    public float moveSpeed;

    //bool used to tell when the gameobject can move
    public bool canMove;

    //The rigidbody2d is an object set here in order to use physics with this game object. 
    //This is very useful when you want the object to have a velocity or gravity or other physics attributes
    private Rigidbody2D myRigidBody;

    // Use this for initialization
    // start is a key function within the Unity engine. Code within this function will only be used once upon 
    // that start of a scene or when the game object that this code i sattached to is initialised
    void Start () {

        // gets the rigidbodys that is attched to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myRigidBody = GetComponent<Rigidbody2D>();
	}

    // Update is called once per frame
    // this is used when continually checking everything happening within the game scene
    void Update () {

        // check whether the gameobject can move
		if(canMove)
        {
            // if the object can move then its velocity of its rigidbody is then set so it will move along the x-axis 
            //at the negative of the movespeed making it go left towards the player given the player always starts along 
            //the left side of the level
            myRigidBody.velocity = new Vector2(-moveSpeed, myRigidBody.velocity.y);
        }
	}
    // used to detect when the game object becomes visible within the scene and once it does become 
    // visible it then is activates its function 
    private void OnBecameVisible()
    {
        //setting the boolean to true allowing it to move
        canMove = true;
    }
    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'KillZone'
        if(collision.tag == "KillZone")
        {
            // it makes it inactive within the scene
            gameObject.SetActive(false);
        }
    }
    // when the object is active within the scene again
    private void OnEnable()
    {
        // we reset it so it no longer is able to move
        canMove = false;
    }
}
// [GAVIN END]
