﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
// This script is used to pause the game while the user is playing allowing them to either continue or exit
public class Pause : MonoBehaviour {

    // variables for instances of the player and levelmanager
    // the pause screen and string to be used to change to the main menu
    public string mainmenu;
    private LevelManager theLevelManager;
    public GameObject pauseScreen;
    private PlayerMovement thePlayer;
	// Use this for initialization
	void Start () {
        // sets the instance of the level manager to that of a gameobject with the levelmanager attached
        theLevelManager = FindObjectOfType<LevelManager>();
        // sets the instance of the playermovemet to that of a gameobject with the Player attached
        thePlayer = FindObjectOfType<PlayerMovement>();
    }
	
	// Update is called once per frame
	void Update () {
        // when the user presses the escape button 
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // checks if the timescale is 0. Therefore the game is frozen
            if(Time.timeScale == 0f)
            {
                //starts the resume function
                resumeGame();
            }
            //otherwise it uses to pause function
            else
            {
                pauseGame();
            }
        }
    }

    //the pause function freezes time and sets the pause screen to active, makes it so the player cannot move
    // and pauses the main game music
    public void pauseGame()
    {
        Time.timeScale = 0f;
        pauseScreen.SetActive(true);

        thePlayer.canmove = false;

        theLevelManager.mainGameMusic.Pause();
    }

    // this function sets the pause screen to inactive
    // unfreezes time, lets the player move again and continues the main game music
    public void resumeGame()
    {
        pauseScreen.SetActive(false);
        Time.timeScale = 1f;
        thePlayer.canmove = true;
        theLevelManager.mainGameMusic.Play();
    }

    // unfreezes time and switchs the scene to the mainmenu
    public void quitToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainmenu);
    }
}
//[GAVIN END]