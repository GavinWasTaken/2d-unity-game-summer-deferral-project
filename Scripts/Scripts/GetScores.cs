﻿
//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using UnityEngine;
using LitJson;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// Test code to be used with the localhost database and 
// scripts to get and send data before implementing the online databsse

public class GetScores : MonoBehaviour {
    List<PlayerScore> playerScores;
    public PlayerScore[] playerScoreData { get; set; }
    public playerscoreCollections a;
    string Url;
    PlayerScore playerscore;
    
    // Use this for initialization
    void Start () {
        Url = "http://localhost:57086/api/PlayerScores/getUserList/gavino";
        playerscore = new PlayerScore();
        
        
    }
    [Serializable]
    public class PlayerScore
    {
        public int Id { get; set; }
        public string PlayerName { get; set; }
        public int score { get; set; }
    }
    [Serializable]
    public class playerlist
    {
        public System.Collections.Generic.List<PlayerScore> ps;
    }
    [Serializable]
    public class playerscoreCollections
    {
        public PlayerScore[] ps;
    }
    public void GetData()
    {
        
        StartCoroutine("Gd");

    }
    public void PostData()
    {
        StartCoroutine("Pd");
    }

    //IEnumerator Gd()
    //{
    //    WWW www = new WWW(Url);
    //    yield return www;
    //
    //    if(www.error != null)
    //    {
    //        string serviceData = www.text;
    //        Debug.Log(serviceData);
    //        JSONArray jsonArray = JSONArray.Parse(www.text);
    //        if (jsonArray == null)
    //        {
    //            Debug.Log("No Data Converted");
    //        }
    //        else if (jsonArray.Length > 0 )
    //        {
    //            playerScores = new List<PlayerScore>();
    //            for(int i = 0; i < jsonArray.Length; i++)
    //            {
    //                playerscore.Id = Convert.ToInt32(jsonArray[i].Obj["Id"].Number);
    //                playerscore.PlayerName = jsonArray[i].Obj["PlayerName"].Str;
    //                playerscore.score = Convert.ToInt32(jsonArray[i].Obj["score"].Number);
    //                
    //            }
    //            
    //            Debug.Log(playerScores);
    //        }
    //    }
    //    else
    //    {
    //        Debug.Log(www.error);
    //    }
    //}

    IEnumerator Pd()
    {
        string url = "http://localhost:57086/api/PlayerScores/{PlayerScore}";
        WWWForm form = new WWWForm();
        form.AddField("Id", 1);
        form.AddField("PlayerName", "Terence");
        form.AddField("score", 111);

        WWW post = new WWW(url, form);

        yield return post;

        if (!string.IsNullOrEmpty(post.error))
        {
            print("Error: " + post.error);
        }
        else
        {
            Debug.Log(post.text);
        }
        //string input = "{Id:3, PlayerName:Peter, score:333}";
        //var headers = new Hashtable();
        //headers.Add("Content-Type", "application/json");
        //byte[] body = Encoding.UTF8.GetBytes(input);
        //WWW t = new WWW("http://localhost:57086/api/PlayerScores", body, headers);
        //
        //yield return t;

    }
    IEnumerator Gd()
    {
        WWW www = new WWW(Url);
        yield return www;
        if(www.error == null)
        {
            string serviceData = www.text;
            Debug.Log(serviceData);

            a = JsonConvert.DeserializeObject<playerscoreCollections>(serviceData);

            Debug.Log(a);
           //object[] jsonobject = new JsonReader.Deserialize()
           //a = JsonUtility.FromJson<playerscoreCollections>(serviceData);
           for(int i = 0; i < a.ps.Length; i++)
           {
               Debug.Log(a.ps[i].PlayerName + ": " + a.ps[i].score);
           }
            
           Debug.Log(a.ps[1].PlayerName);
        }
        else
        {
            Debug.Log(www.error);
        }
        
    }


    // Update is called once per frame
    void Update () {
		
	}


    //public IEnumerator ProcessJson(string jsonString)
    //{
    //    p = new List<PlayerScore>();
    //    WWW get = new WWW("http://localhost:57086/api/getUserList/gavino");
    //    yield return get;
    //    if (get.error == null)
    //    {
    //        JsonReader reader = new JsonReader(get.text);
    //        while(reader.Read())
    //        {
    //            string type = reader.Value != null ? reader.Value.GetType().ToString() : "";
    //            Debug.Log("" + reader.Value);
    //        }
    //    }
    //    else
    //    {
    //        Debug.Log("Error: " + get.error);
    //    }
    //    
    //    //JsonData jsonval = JsonMapper.ToObject(jsonString);
    //    //PlayerScore playerscore;
    //    //playerscore = new PlayerScore();
    //    //playerscore.PlayerName = jsonval["PlayerName"].ToString();
    //    //playerscore.score = jsonval["score"].ToString();
    //    //
    //    //
    //    //for(int i = 0; i < jsonval["ps"].Count; i++)
    //    //{
    //    //
    //    //}
    //
    //}
        //public void getScoreByPlayerName()
   //{
   //    p = new List<PlayerScore>();
   //    WWW get = new WWW("http://localhost:57086/api/getUserList/gavino");
   //    yield return get;
   //    string json = get.text;
   //    playerlist d = JsonUtility.FromJson<playerlist>(json);
   //    Debug.Log(json);
   //    //Debug.Log(testString);
   //    Debug.Log(d.ps.Count);
   //    Debug.Log(d.ps.ToList<PlayerScore>());
   //
   //    //HttpClient client = new HttpClient();
   //    //client.GetString(new Uri("http://localhost:57086/api/PlayerScores/getUserList/gavino"), (r) =>
   //    //    {
   //    //        //HttpResponseMessage<PlayerScore> response = r.Data.ToArray;
   //    //        StringBuilder t = new StringBuilder(r.Data);
   //    //
   //    //        //string testString = t.Replace("[", string.Empty).Replace("]", string.Empty).ToString();
   //    //
   //    //        string jsonString = JsonUtility.ToJson();
   //    //        //PlayerScore p = new PlayerScore();
   //    //        playerlist d = JsonUtility.FromJson<playerlist>(r.Data);
   //    //        //p = JsonUtility.FromJson<PlayerScore>(r.Data);
   //    //        Debug.Log(r.Data);
   //    //        //Debug.Log(testString);
   //    //        Debug.Log(d.ps.Count);
   //    //        Debug.Log(d.ps.ToList<PlayerScore>());
   //    //        foreach(PlayerScore player in d.ps)
   //    //        {
   //    //            p.Add(player);
   //    //            Debug.Log(player);
   //    //        }
   //    //        
   //    //});
   //}
    

}

//[GAVIN END]