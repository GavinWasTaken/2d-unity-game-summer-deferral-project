﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
// The level manager is a key stone to every level within the unity game
// It is in charge of multiple functions and keeps track of vital information.
// for everyplayable level there is a level manager present to be reference and used with other scripts
public class LevelManager : MonoBehaviour {

    //variables to be used with the respawn of the player
    public float waitToRespawn;
    public PlayerMovement thePlayer;
    public GameObject DeathParticle;

    // instance of the infiniterunnerplayercontroller
    public InfiniteRunnerPlayerController theRunner;

    //variables used with the collection of coins and other scoring objects
    public int coinCount;
    public int coinCountBonus;
    public Text coinText;
    public GameObject[] collectibles;

    // scores texts to be used to keep track of the score
    public Text scoreText;
    public Text endLevelScore;

    // images to be used within the ui to keep track of the players health
    public Image heart1;
    public Image heart2;
    public Image heart3;

    // different sprites for indications of health
    public Sprite heartFull;
    public Sprite heartHalf;
    public Sprite heartEmpty;

    //player health variables
    public int maxHealth;
    public int healthCount;
    private bool respawning;

    // instances of enemy used scripts
    public EnemyHealthManager enemyHealthManager;
    public ResetWhenRespawn[] objectsToReset;
    public ResetPatrollingEnemies[] patrolstoreset;

    // inviciblity checkfor the player
    public bool invincible;

    // text and ints used to hold the amount of lives the plaayer has
    public Text livesText;
    public int startingLives;
    public int currentLives;

    // the gameover screen to be shown upon losss of all lives
    public GameObject gameOverScreen;

    // collectible sound effects
    public AudioSource coindSfx;
    public AudioSource healthPickUpSfx;

    // game music
    public AudioSource mainGameMusic;
    public AudioSource gameOverMusic;
    public AudioSource levelcompleteMusic;

    // instance of the checkpoints script
    private checkpointController theCheckPoints;

    // the players score
    public int score;
    // Use this for initialization

    void Start () {

        // the instances are set to component sna dna other game objects within the scene
        thePlayer = FindObjectOfType<PlayerMovement>();
        theRunner = FindObjectOfType<InfiniteRunnerPlayerController>();
        enemyHealthManager = GetComponent<EnemyHealthManager>();
        theCheckPoints = FindObjectOfType<checkpointController>();

        // health and lives are set to starting lives and health
        currentLives = startingLives;
        healthCount = maxHealth;

        // gets and fills the collectibles array with all the objects in the scene with the 'Collectivble' tag
        collectibles = GameObject.FindGameObjectsWithTag("Collectible");

        //sets the arrays instance to the objects within the scene with the same script
        objectsToReset = FindObjectsOfType<ResetWhenRespawn>();
        patrolstoreset = FindObjectsOfType<ResetPatrollingEnemies>();

        // getting the playerprefs for the coin count and then setting the ui text to show the coin count
        if(PlayerPrefs.HasKey("CoinCount"))
        {
            coinCount = PlayerPrefs.GetInt("CoinCount");
        }
        coinText.text = "Coins: " + coinCount;
        
        // getting the player pref for the current amount of player lives and then
        // showing the current amount of player lives by setting the ui to that amount
        if (PlayerPrefs.HasKey("PlayerLives"))
        {
            currentLives = PlayerPrefs.GetInt("PlayerLives");
        }
        livesText.text = " x" + currentLives;

        // getting the player pref for the score  and then
        // showing the score by setting the ui to that amount
        if (PlayerPrefs.HasKey("Score"))
        {
            score = PlayerPrefs.GetInt("Score");
        }

        // if the there are no player prefs we then set the curret lives to the starting lives 
        // and the score to zero and then sets the ui text for the score to that score
        else
        {
            currentLives = startingLives;
            score = 0;
        }
        scoreText.text = "Score: " + score;

    }
	
	// Update is called once per frame
	void Update () {
        //keeping track of the players health and if it reach zero or less than 
        //zero the respawn function will begin and respawning wil be set to true
		if(healthCount <= 0 && !respawning)
        {
            Respawn();
            respawning = true;
        }
        //keeping track of the the coin count and if the coin count increase by more than 1000
        // then the player will earn themselves another life and the count will be reset to zero
        if(coinCountBonus >= 100)
        {
            currentLives += 1;
            livesText.text = " x" + currentLives;
            coinCountBonus -= 100;
        }
	}
    // the respawn functioon if used to reset the players data and position back to either the start of the level or the last checkpoint
    public void Respawn()
    {
        currentLives -= 1;
        livesText.text = " x" + currentLives;
        //if the current amount of lives is not zero or less than zero the respawn coroutine will begin
        if(currentLives > 0)
        {
            StartCoroutine("RespawnCo");
        }
        // otherwise the player will no longer be active and the gameover will be presented to them
        else
        {
            thePlayer.gameObject.SetActive(false);
            gameOverScreen.SetActive(true);
            // also the main game music will stop
            // and the gammeover music will play
            mainGameMusic.Stop();
            gameOverMusic.Play();
        }
        
        
    }

    // this coroutine is used to reset all the nessary data for the player to start over again
    public IEnumerator RespawnCo()
    {
        // this checkpoint if the player has reach a checkpoint within the game 
        // if they have the respawn position will be set to the position of this checkpoint
        if (!theCheckPoints.checkpointActive)
        {
            // all the collectible within the game will be looped through and reset as well
            foreach (GameObject collectible in collectibles)
            {
                collectible.SetActive(true);
            }
        }

        // the player will no longer have a parent object
        thePlayer.transform.parent = null;
        // and they will be inactive
        thePlayer.gameObject.SetActive(false);
        
        // a death particle effect will be spawned in to the last position of the player
        // wheere they died to better indicate there death
        Instantiate(DeathParticle, thePlayer.transform.position, thePlayer.transform.rotation);

        // a couple of second will be given you the player before respawning so that
        // they may learn how they died
        yield return new WaitForSeconds(waitToRespawn);

        // during the respawn phase the health will be reset to full
        // they are no longer respawning
        // and the UpdateHeartMeter funcrion will be implemented
        healthCount = maxHealth;
        respawning = false;
        UpdateHeartMeter();

        // the score will be equal to the last score the player had when they reached the checkpoint
        // the same goes for their coincount
        score = theCheckPoints.newScore;
        coinCount = theCheckPoints.newCoinCount;

        // the ui of the score will be updated to match their new score
        coinText.text = "Coins: " + coinCount;
        scoreText.text = "Score: " + score;
        // they will now be at the position of the checkpoint
        thePlayer.transform.position = thePlayer.respawnPosition;
        thePlayer.gameObject.SetActive(true);

        // the array of objects left to be reset will be looped through and set back to active
        // enemies healths will be set backto full too
        for(int i = 0; i < objectsToReset.Length; i++)
        {
            objectsToReset[i].gameObject.SetActive(true);
            objectsToReset[i].reset();
            enemyHealthManager.enemyHealth = enemyHealthManager.maxHealth;
        }
        // patrolling enemies will also be reset
        for (int i = 0; i < patrolstoreset.Length; i++)
        {
            patrolstoreset[i].gameObject.SetActive(true);
            patrolstoreset[i].reset();
        }
    }

    // this function is used to update the coin count by the amount given
    // this also updates the ui too
    public void AddCoins(int coinsToAdd)
    {
        coinCount += coinsToAdd;
        coinCountBonus += coinsToAdd;
        coinText.text = "Coins: " + coinCount;
        coindSfx.Play();
    }

    // this funciton updates the score and the ui as well much like the
    // 'AddCoins' function
    public void AddScore(int scoreToGive)
    {
        score += scoreToGive;
        scoreText.text = "Score: " + score;
    }

    // this function is used to update the players health by subtracting the amount given from the players health
    // but only when they are not set to invincible
    public void hurtPlayer(int damageToTake)
    {
        if(!invincible)
        {
            healthCount -= damageToTake;
            UpdateHeartMeter();
            // uses the players knockback function in the playermovement script when they lose health
            thePlayer.Knockback();
            // plays the players hurt sound effect
            thePlayer.hurtsfx.Play();
        }
    }

    // this functiion adds health back to the player based on the amount given
    // it will not add more than the maximum amount of health though
    // and will then implement 'UpdateHeartMeter'
    public void GiveHealth(int healthToBeGiven)
    {
        healthCount += healthToBeGiven;
        // plays the audio pick up health clip
        healthPickUpSfx.Play();
        if (healthCount > maxHealth)
        {
            healthCount = maxHealth;
        }
        UpdateHeartMeter();
        
    }

    // this function is used to update the visual representation of the players health, which is show as a bunch of full hearts, half hearts and empty hearts
    public void UpdateHeartMeter()
    {
        // there are three full heart across the top of the screen each full heart is 2 points
        // each half heart is 1 point and every empty heart is 0 points
        // this switch uses the players maximum health of 6 to set the different sprites for each of the three sprites
        switch(healthCount)
        {
            case 6:
                heart1.sprite = heartFull;
                heart2.sprite = heartFull;
                heart3.sprite = heartFull;
                return;
            case 5:
                heart1.sprite = heartFull;
                heart2.sprite = heartFull;
                heart3.sprite = heartHalf;
                return;
            case 4:
                heart1.sprite = heartFull;
                heart2.sprite = heartFull;
                heart3.sprite = heartEmpty;
                return;
            case 3:
                heart1.sprite = heartFull;
                heart2.sprite = heartHalf;
                heart3.sprite = heartEmpty;
                return;
            case 2:
                heart1.sprite = heartFull;
                heart2.sprite = heartEmpty;
                heart3.sprite = heartEmpty;
                return;
            case 1:
                heart1.sprite = heartHalf;
                heart2.sprite = heartEmpty;
                heart3.sprite = heartEmpty;
                return;
            case 0:
                heart1.sprite = heartEmpty;
                heart2.sprite = heartEmpty;
                heart3.sprite = heartEmpty;
                return;         
            default:            
                heart1.sprite = heartEmpty;
                heart2.sprite = heartEmpty;
                heart3.sprite = heartEmpty;
                return;
        }
    }

    // this function updates the amount of lives the player has by the amount given
    // and then updates the ui
    public void addLives(int livesToAdd)
    {
        // plays the audio pick up health clip
        healthPickUpSfx.Play();
        currentLives += livesToAdd;
        livesText.text = " x" + currentLives;
    }
}
//[GAVIN END]