﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this scripts serves the same purpose as the PatrollingEnemy2Points script 
// with the difference of using the object to move towards one position and once reached to change to move towards 
// the other position
public class MovingObject : MonoBehaviour {

    public GameObject objectToMove;
    public Transform startPosition;
    public Transform endPosition;

    public float moveSpeed;

    private Vector2 currentTarget;
    // Use this for initialization
    void Start()
    {
        currentTarget = endPosition.position;
    }

    // Update is called once per frame
    void Update()
    {
        objectToMove.transform.position = Vector2.MoveTowards(objectToMove.transform.position, currentTarget, moveSpeed * Time.deltaTime);
        if(objectToMove.transform.position == endPosition.position)
        {
            currentTarget = startPosition.position;
        }
        if (objectToMove.transform.position == startPosition.position)
        {
            currentTarget = endPosition.position;
        }
    }
}
//[GAVIN END]