﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script is used with the archer enemy type. An enemie that is stays in one point and fires arrows at the player in order to destory the player
public class Archer : MonoBehaviour {

    //The rigidbody2d is an object set here in order to use physics with this game object. 
    //This is very useful when you want the object to have a velocity or gravity or other physics attributes
    public Rigidbody2D myRigidbody;

    // We create a playermovement instace which references the players controller script
    public PlayerMovement thePlayer;

    //We reference the animator so that we can code functions to do with this gameobjects animator and animations.
    public Animator animation;

    // The transform or the firepoint which the arrow will come from.
    // (Every gameobject has a transforms which holds the objects positions within the scee, its rotation of the object itsself and the scale of the object.)
    public Transform firepoint;

    // this  Is the gameobject which will the arrow which will be fired from the archer at the player.
    //The arrow is an object in itself
    public GameObject arrow;

    // A float for the delay of when the enemy will fire the arrow.
    //This is needed so the archer doesnt fire continuely without giving the player a chance to evade its arrows
    public float firedelay;

    // this float interacts with the firedelay in order to create the fire delay
    public float firecounter;

    // this is a float that works to create a distance in which the range circle for the archer to detect the enemy
    public float shootRange;

    // create an area that can be seen in the scene but not the game itself and 
    // allows us to measure the radius in which the archer will use to detect the player
    public LayerMask shootLayer;

    // a bool which will be used to tell functions when the player in within the radius the archer has.
    public bool inShootRange;

    // a bool used to detect when the player is on the right side of the archer and when is not(when is on the left)
    public bool isPlayerRight;

    // Use this for initialization
    void Start () {

        // gets the rigidbodys that is attched to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myRigidbody = GetComponent<Rigidbody2D>();

        // find objects within the scene that have the playermovement sccript and assigns an instance of them to the 'thePlayer' instance.
        // since there is only one player within the scene at a time then this instance will be used
        thePlayer = FindObjectOfType<PlayerMovement>();

        // sets the animator that is attach to the gameobject to the instance of 'animation'
        animation = GetComponent<Animator>();

        // sets the firecounter to equal the firedelay given at the start
        firecounter = firedelay;

    }
	
	// Update is called once per frame
    // this is used when continually checking everything happening within the game scene
	void Update () {

        // the firecount is reduced by Time.deltatime which is a simple count of time
        firecounter -= Time.deltaTime;

        // check is the player is on the right of the archer and is the players position on the x axis is less that this objects position allong the x axis
        if (isPlayerRight  && thePlayer.transform.position.x < transform.position.x)
        {

            // the localscale is used to changed the scale of the gameobject and by restting the scales x value by multiplying it by -1 we effectively flip it so it is facing the player
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);

            // we then reset the boolean 
            isPlayerRight = false;
        }
        // check is the player is on the left of the archer and is the players position on the x axis is more than this objects position allong the x axis
        if (!isPlayerRight  && thePlayer.transform.position.x > transform.position.x)
        {
            // the localscale is used to changed the scale of the gameobject and by restting the scales x value by multiplying it by -1 we effectively flip it so it is facing the player
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);

            // like before the boolean is then reset
            isPlayerRight = true;
        }

        // Checks if a collider falls within a circular area of the gameobject with the radius of the shootrange which was set earlier
        // and if so sets the 'isShootRange' to true
        inShootRange = Physics2D.OverlapCircle(transform.position, shootRange, shootLayer);

        // is the 'isShootRange' is true and the firecounter has reach zero or les then zero from its countdown mentioned before 
        //then it can start firing an arrow
        if (inShootRange && firecounter <= 0)
        {

            //inRange = false;
            // its plays an animations from its animator by setting one of its parameters which is a bool called attack to true
            // is is worth mentioing that within the animator an event can be set at key points of the animator which activate a method of code
            // within the attack animation it activates a event which called the spawnarrow method below
            animation.SetBool("attack", true);

            // it then resets the firecounter so it will have to wait before it can fire again
            firecounter = firedelay;
        }

        // if the inShootRnage boolean is false
        if (!inShootRange)
        {

            // we reset the animation by settings its parameter to false making it exit this animation
            animation.SetBool("attack", false);

        }


    }

    // when the object is selected withing the scene during testing and configuring it displays a visual gizmo
    private void OnDrawGizmosSelected()
    {

        // the gizmo drawn is a circle with the same radius as the layermask mentioned before.
        // This helps give a visual representation or the shootrange area that will help us set up our archer better
        Gizmos.DrawSphere(transform.position, shootRange);

    }

    // this method spawn the arrow gameobject
    void spawnArrow()
    {

        // it creates an instance of the arrow object at the firepoint position with its firepoint rotation
        Instantiate(arrow, firepoint.position, firepoint.rotation);

    }
}
// [GAVIN END]
