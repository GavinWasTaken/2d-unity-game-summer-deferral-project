﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// used to make object, usually enemies respawn and rest upon a player respawn or level restart
public class ResetWhenRespawn : MonoBehaviour {

    // variables used to getinstances of the enemy health manager, the objects starting position, 
    // its start rotation, the direction it should be facing
    // and its 2d rigidbody
    public EnemyHealthManager healthmanager;
    private Vector2 startPos;
    private Quaternion startRot;
    private Vector2 startLocalScale;
    private Rigidbody2D myrigidbody;
	// Use this for initialization
	void Start () {
        //sets the intance of the enemy health manager to a component attached to the gameobject
        healthmanager = GetComponent<EnemyHealthManager>();

        //setting the startposition to the current position
        startPos = transform.position;

        //setting the start rotation to the current rotation
        startRot = transform.rotation;

        //setting the start localscale to the current localscale
        startLocalScale = transform.localScale;
        // checks if the object has a 2d rigidbody and if it does the sets the rigidbody2d instance
        // to that rigid body
        if (GetComponent<Rigidbody2D>() != null)
        {
            myrigidbody = GetComponent<Rigidbody2D>();
        }
	}
    // function to reset the gameobject back to the positions and rotations
    // it also reset the enemy health of the object and sets its velocity to zero
    public void reset()
    {
        healthmanager.enemyHealth = healthmanager.maxHealth;
        transform.position = startPos;
        transform.rotation = startRot;
        transform.localScale = startLocalScale;

        if(myrigidbody != null)
        {
            myrigidbody.velocity = Vector2.zero;
        }
        //healthmanager.enemyHealth = healthmanager.maxHealth;
    }
}
//[GAVIN END]