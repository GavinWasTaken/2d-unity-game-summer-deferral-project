﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// script used for rotating around a certain point within the game
// not yet used within the game but successful in test made
public class RotateAround : MonoBehaviour {

    // settings for the object rotation speed, the object at which to rotate around
    // also added boundaries to make the object not make a full rotation if needed
    public float speed;
    public Transform ObjectToRotateAround;
    public Transform boundary;
    public bool changeDirection;
    private Vector3 zAxis = new Vector3(0, 0, 1);

    // using the functions within at set intervals
    private void FixedUpdate()
    {
        // if the y position of the obeject reachs les than the boundarys y position then it will reverse its rotation
        if (transform.position.y < boundary.position.y)
        {
            changeDirection = true;
        }
        // if the direction  is change the speed is reversed making it go in the opposite
        // direction
        if (changeDirection)
        {
            speed = -speed;
            changeDirection = false;
        }
        // sets the obejct to rotate aroudn and rotates around it
        transform.RotateAround(ObjectToRotateAround.position, zAxis, speed);

    }

}
//[GAVIN END]