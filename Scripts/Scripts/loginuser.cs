﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//this scripts allos the player to login into their account if they enter the correct credentials
public class loginuser : MonoBehaviour {

    // variables for the usernames, passwords and if they are logged in or not
    public string inputUsername;
    public string inputPassword;
    public bool loggedIn;

    // input field where the user can input their own username and password
    public InputField loginUsername;
    public InputField loginPassword;

    // message to appear depending on whether the user has been succesfully logged in or failed to log in
    public GameObject loginFailedMessage;
    public GameObject loginSuccessMessage;

    // the amount of time the emssage remains on the screen
    public float messageTime;

    // the url for the online php script that is used in conjunction with the online database
    string loginUrl = "https://gavinhickeyty4.000webhostapp.com/verifyUser.php";

    // theis function gathers the text from the input fields and starts the coroutine using this data
   public void loginCheck()
    {
        inputUsername = loginUsername.text;
        inputPassword = loginPassword.text;
        StartCoroutine(LoginToDb(inputUsername, inputPassword));
        
    }
    // this coroutine passes in the username and password and then uses them as fields through a www form
    // which is a helper form used to help post data to web servers using the www class
    IEnumerator LoginToDb(string username, string password)
    {
        //create an instanc of the www form 
        WWWForm form = new WWWForm();
        // added the username and password data to the right fields 
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);

        // using the www form and url to the online php script
        WWW www = new WWW(loginUrl, form);
        yield return www;
        Debug.Log(www.text);
        //the online script is to respond with a specific message upon a succefully login.
        // this check to see if the response contain that response if so then it is sucessful, 
        // they will recieve the successful message and the user is logged in
        // if not they are not and will receive the failed message
        if (www.text.Contains("login success"))
        {
            loggedIn = true;

            //activates the log success message
            loginSuccessMessage.SetActive(true);

            // waits a the designated amount of time
            yield return new WaitForSeconds(messageTime);
            
            //deactivates messsage
            loginSuccessMessage.SetActive(false);
            // sets the player pref of the ussername to the newly logged in username
            PlayerPrefs.SetString("UserName", username);
        }
        else
        {
            loggedIn = false;

            //activates the log fail message
            loginFailedMessage.SetActive(true);

            // waits a the designated amount of time
            yield return new WaitForSeconds(messageTime);

            //deactivates messsage
            loginFailedMessage.SetActive(false);
            
        }
    }
}
//[GAVIN END]