﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// this is a small script useed to open and close a screen type that doesn't scpecifically have to be 
// a login screen
public class Login : MonoBehaviour {
    public GameObject loginScreen;

    // activate the login screen
    public void openLoginScreen()
    {
        loginScreen.SetActive(true);
    }
    //deactivate the login screen
    public void closeLoginScreen()
    {
        loginScreen.SetActive(false);
    }

}
//[GAVIN END]