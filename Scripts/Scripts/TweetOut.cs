﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
// script used with the tweet out button on the main menu allowing the user to use the twitter 
// social media platform to make an automated tweet with a link to a downloadable copy of the game
public class TweetOut : MonoBehaviour {

    public string tweetMessageBody;
    public string tweetdescription ;
    private string tweetUrl = "http://twitter.com/intent/tweet";

    public void tweetOut()
    {
        // Opens the url and leaves the game as well going tot he url set in the variable with the strings used as the decription and body of the message 
        Application.OpenURL(tweetUrl + "?text=" + WWW.EscapeURL(tweetMessageBody + "\n" + tweetdescription + "\n"));
    }
}
//[GAVIN END]