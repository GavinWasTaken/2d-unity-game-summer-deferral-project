﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is used to activate a bomb attack when the player is detect near to the game object
public class detectandbombplayer : MonoBehaviour {
    // set an instance of the animator
    private Animator animation;

    // an instance of rigidbody2d
    public Rigidbody2D myrb;

    // the bomb gameobject
    public GameObject bomb;

    // the point at which the bomb is to be spawned
    public Transform bombPoint;

    // time the animation will take
    public float animationTime;
    // Use this for initialization
    void Start()
    {
        //setting the animator to that of the animator component atteh to the gameobject
        animation = GetComponent<Animator>();
        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myrb = GetComponent<Rigidbody2D>();
    }

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'Player'
        if (collision.tag == "Player")
        {
            // the velocity of the rigidbody2d on the game object is set to 0
            Vector3 v = myrb.velocity;
            v.y = 0.0f;
            v.x = 0.0f;
            myrb.velocity = v;

            // animations paramter is set to true activating the objects attack animation
            animation.SetBool("attack", true);
            //Starts the named method after the animationtime
            Invoke("spawnBomb", animationTime);
            //Instantiate(bomb, bombPoint.position, bombPoint.rotation);
        }
       
    }

    // this method spawns in the bomb gameobject and makes sure that its velocity is also set to zero
    public void spawnBomb()
    {
        
        GameObject b = (GameObject)Instantiate(bomb, bombPoint.position, bombPoint.rotation);
        b.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
}
//[GAVIN END]