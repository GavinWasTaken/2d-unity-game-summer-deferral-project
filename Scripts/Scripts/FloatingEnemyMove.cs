﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//this script is used for the floating enmies such as the mages for which they have to radius to search for an enmye then once they 
// detect one they move towards the player then fires dark orbs at the player when the player is detected within the second redius
public class FloatingEnemyMove : MonoBehaviour {

    // the gameobjects rigidbody2d
    public Rigidbody2D myRigidbody;

    //instance of the player
    public PlayerMovement thePlayer;

    // animator of the gameobject
    public Animator animation;

    // the transform of the firepoint
    public Transform firepoint;

    // the moving speed of the gameobject
    public float movespeed;

    // the float of the distance
    public float floatdistance;

    // the dark orb gameobject
    public GameObject darkOrb;

    //delay for the fireing of dark orbs
    public float firedelay;

    // a counter which works with the firedelay 
    public float firecounter;

    // the range of the player
    public float playerRange;

    // range to shoot in
    public float shootRange;

    // create an area that can be seen in the scene but not the game itself and 
    // allows us to measure the radius in which the mage will use to detect the player and move towards the player
    public LayerMask playerLayer;

    // create an area that can be seen in the scene but not the game itself and 
    // allows us to measure the radius in which the mage will use to detect the player and shoot towards
    public LayerMask shootLayer;

    // bool for if the player is within the detection range
    public bool inRange;
    // bool for if the player is within the shooting range
    public bool inShootRange;

    // checks if the player is to the right or left 
    public bool isPlayerRight;
    // Use this for initialization
	void Start () {
        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myRigidbody = GetComponent<Rigidbody2D>();
        // sets this instance to the object within the scene with the playermovement script
        thePlayer = FindObjectOfType<PlayerMovement>();

        // set the animation to the the animator component attached to the gameobject
        animation = GetComponent<Animator>();
        // sets the fire delay to the fire counter
        firecounter = firedelay;
    }
	
	// Update is called once per frame
	void Update () {
        //firecounter counts down
        firecounter -= Time.deltaTime;

        //The game object uses the position o the theplayer instance's x-axis position to help set the 'isPlayerRight' boolean
        // and to set the gameobjects localscale which will make the gameonject face towards to player
        if (isPlayerRight && thePlayer.transform.position.x < transform.position.x)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            isPlayerRight = false;
        }
        if (!isPlayerRight && thePlayer.transform.position.x > transform.position.x)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            isPlayerRight = true;
        }

        //using this range that when the player enters the circle the 
        //game object will then move towards the player until then reach the next range
        inRange = Physics2D.OverlapCircle(transform.position, playerRange, playerLayer);
        if(inRange && !inShootRange)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(thePlayer.transform.position.x, thePlayer.transform.position.y + floatdistance) , movespeed * Time.deltaTime);
        }
        // once add this range the gameobject wil then start to spawn in the enemy 
        // dark orbs once the firecounter eaches zero or less and then reset the firecounter
        inShootRange = Physics2D.OverlapCircle(transform.position, shootRange, shootLayer);
        if(inShootRange && firecounter <=0 )
        {

            animation.Play("attack");
            Instantiate(darkOrb, firepoint.position, firepoint.rotation);
            
            firecounter = firedelay;
        }
    }
    // when the object is selected withing the scene during testing and configuring it displays a visual gizmo
    private void OnDrawGizmosSelected()
    {
        // the gizmo drawn is a circle with the same radius as the layermask mentioned before.
        // This helps give a visual representation or the playerrange area that will help us set up our mage better
        Gizmos.DrawSphere(transform.position, playerRange);
    }
    
}
//[GAVIN END]