﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this is the script given to the arrow game object to change its behavior to an arrow type
public class EnemyArrow : MonoBehaviour {

    // instance of a rigibody2d
    public Rigidbody2D myrigidbody;

    // instance of the impart particle 
    public GameObject impactParticle;

    // instance of a vector for the players last position
    private Vector3 lastPlayerPos;

    // integer of the damge to give 
    public int damageToGive;

    //float of the arrows speed
    public float speed;
    
    //float for the minimum distance the arrow can travel 
    public float mindistance;

    //maximum hieght of the arrows shot
    public float shotheight;

    //instance of the player
    private PlayerMovement thePlayer;

    // instance of the level manager
    private LevelManager theLevelManager;
    
    // Use this for initialization
    void Start () {
        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myrigidbody = GetComponent<Rigidbody2D>();

        // sets the instance of the level manager to that of a gameobject with the levelmanager attached
        theLevelManager = FindObjectOfType<LevelManager>();

        // sets the instance of the thePlayer to that of a gameobject with the playermovement script attached
        thePlayer = FindObjectOfType<PlayerMovement>();

        // sets the distance between the player and the arrow
        float distbetween = Vector3.Distance(thePlayer.transform.position, gameObject.transform.position);
        // Debug.log is used here to display the distace between within unity console during runtime
        Debug.Log(distbetween);
        
        // sets the last position the player was in to the players position with the added shot height on the y-axis
        lastPlayerPos = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y + shotheight, thePlayer.transform.position.z);

        // this change to arrows scale to flip it if the arrows x-axis position is less that the players
        if (thePlayer.transform.position.x > gameObject.transform.position.x)
        {
            
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        }
        //else {
        //    myrigidbody.velocity = new Vector2(speed, myrigidbody.velocity.y);
        //}

    }

    // this fixed update used the code within it every constant time frame unllike the normal update whihc is continous
    private void FixedUpdate()
    {
        // moves this gameobject towards the players last known position
        this.transform.position = Vector2.MoveTowards(transform.position, lastPlayerPos, speed * Time.deltaTime);
    }

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // if the gameobjects collider comes into contactt with another collider whos tag is 'Player'
        if (collision.collider.tag == "Player")
        {
            // uses the levelmanagers hurtplayer function with the damageToGive integer
            theLevelManager.hurtPlayer(damageToGive);
        }
        // spawns the impact particle at the gameobjects position and rotation
        Instantiate(impactParticle, transform.position, transform.rotation);
        // destroys the gameobject removing it from  the scene
        Destroy(gameObject);
    }
}
//[GAVIN END]