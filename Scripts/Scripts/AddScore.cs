﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

// This script is used within the game to add points to our score with each object it is attached to.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScore : MonoBehaviour {
    // a private instance of the levelmanager script
    private LevelManager theLevelManager;

    // A public integer used to set the score to give per object. This can be set in the inspector of the unity engine allowing to customize object with differing scores to give.
    public int scoreToGive;
	// Use this for initialization
    // start is a key function within the Unity engine. Code within this function will only be used once upon that start of a scene or when the game object that this code i sattached to is initialised
	void Start () {
        // The code finds an object within the games hierarchy that has a level manager script attached. Since there is only one level manager object per game scene we onl need to search for one object. This is then assigned to the variable 'theLevelManager'
        theLevelManager = FindObjectOfType<LevelManager>();
	}

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the collision object has a tag that is equal to 'Player' the code will continue ahead. This is used so that only the player can interact and add score.
        // Stopping other objects like enemies from adding scores
        if (collision.tag == "Player")
        {
            // using the level manager instance the code will call upon a function to add the score given integer to add the score
            theLevelManager.AddScore(scoreToGive);
            // once the object has added the score we do not want it to still exist so the player can possibly spam these object and gain unlimited points.
            // So we then set its activity to false rendering it inactive within the game but still with the possiblity to be reset to true and reactivate it.
            // This way we can reuse these objects 
            gameObject.SetActive(false);
        }
    }
}
// [GAVIN END]
