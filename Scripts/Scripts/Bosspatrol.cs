﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

// This script is a copy of the chasepplayer script.
// reason for making and using a copy is the boss is not a typicial enemy type so if 
// need changes were to be made then we wouldn't want to effect the other enemy gameobjects to using that script
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bosspatrol : MonoBehaviour {
    public PlayerMovement thePlayer;
    public Rigidbody2D myrigidbody;

    public float jumpspeed;
    public float movespeed;
    public bool isPlayerRight;
    public bool playerIsAbove = false;
    public bool jump = false;
    public bool canMove;
    public float distanceBetweenPlayerAndPlatform;

    // Use this for initialization
    void Start()
    {
        myrigidbody = GetComponent<Rigidbody2D>();
        thePlayer = FindObjectOfType<PlayerMovement>();
        canMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            if (isPlayerRight && thePlayer.transform.position.x < transform.position.x)
            {
                transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
                isPlayerRight = false;
            }
            if (!isPlayerRight && thePlayer.transform.position.x > transform.position.x)
            {
                transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
                isPlayerRight = true;
            }

            transform.position = Vector2.MoveTowards(transform.position, new Vector2(thePlayer.transform.position.x, 0), movespeed * Time.deltaTime);
            if (thePlayer.transform.position.y > transform.position.y + 1)
            {
                playerIsAbove = true;
            }
            else
            {
                playerIsAbove = false;
            }
            if (jump)
            {

                myrigidbody.velocity = new Vector2(myrigidbody.velocity.x, jumpspeed);
                jump = false;
            }
            else
            {

                myrigidbody.velocity = new Vector2(myrigidbody.velocity.x, myrigidbody.velocity.y);
            }
        }
        else
        {
            return;
        }
    }
    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            jump = true;
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            jump = true;
        }
    }

    private void OnBecameVisible()
    {
        canMove = true;
    }
}
// [GAVIN END]