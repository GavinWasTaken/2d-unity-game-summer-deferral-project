﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is soley used to call upon a fucntion within the level manager 
// this makes things simplier for the developmen of the game
public class HurtPlayer : MonoBehaviour {

    private LevelManager theLevelManager;
    public int damageToGive;
	// Use this for initialization
	void Start () {
        theLevelManager = FindObjectOfType<LevelManager>();
	}
	
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contactt with another collider whos tag is 'Player'
        if (collision.tag == "Player")
        {
            // uses the level managers hurtPlayer method with the integer given for the damage to give to the player
            theLevelManager.hurtPlayer(damageToGive);
        }
    }
}
//[GAVIN END]