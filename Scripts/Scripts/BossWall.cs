﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// This is a script to be used to remove the wall once the player has defeated the enemy allowing them to continure on and complete the level
public class BossWall : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        // continually checking if there is a game which  has a gameobject with the 'bosshandhealthmanager' script 
		if(GameObject.FindObjectOfType<BossHandHealthManager>() == null)
        {
            // if its null we set the wall to be inactive within the game
            gameObject.SetActive(false);

        }
        else
        {
            //otherwise we keep the object as being active within the scene
            gameObject.SetActive(true);
        }
	}
}
// [GAVIN END]