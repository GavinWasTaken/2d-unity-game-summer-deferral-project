﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// script used to set the players boolean value for when they are on a ladder
public class Ladder : MonoBehaviour {
    //instance of the playermovement script
    private PlayerMovement thePlayer;
	// Use this for initialization
	void Start () {
        // set the instance to a gameboject component in the scene with the playermovement script
        thePlayer = FindObjectOfType<PlayerMovement>();
	}


    // OntriggerEnter2D is used with collders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'PLayer'
        if (collision.tag == "Player")
        {
            // sets the onladder playermovement variable to true
            thePlayer.onLadder = true;
        }
    }
    // OntriggerExit2D is used with collders within the game exit one another colliders
    private void OnTriggerExit2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'PLayer'
        if (collision.tag == "Player")
        {
            // sets the onladder playermovement variable to false
            thePlayer.onLadder = false;
        }
    }
}
//[GAVIN END]
