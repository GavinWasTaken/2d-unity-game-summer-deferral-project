﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is used for the players melee attack 
public class MeleeAttack : MonoBehaviour {

    // variables of the sword sound effect, the damage to inflict upon the enemy
    public AudioClip sword;
    public EnemyHealthManager ehm;
    public int damageToGive;
    // Use this for initialization

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'Enemy'

        if (collision.tag == "Enemy")
        {
            // use the enemyhealthmanagers 'ApplyDamage' function to iinflict damge on the enemy with the set damage the melee attack will give
            collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
        }
        // if the object it connects with is a projectile thenthe project 
        // will be set to inactive giving the player a ability to deflect incoming enemy projectiles
        if(collision.tag  == "Projectile")
        {
            collision.gameObject.SetActive(false);
        }

        if(collision.tag == "Boss")
        {
            // use the BossHandHealthManager 'ApplyDamage' function to iinflict damge on the boss with the set damage the melee attack will give
            collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);

        }


    }
}
//[GAVIN END]