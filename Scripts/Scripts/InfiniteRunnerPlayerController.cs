﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//this script is an exact copy of the playermovement script bbut has been modified to serve the purpose of the infinite runner.
// these change will be outlined
public class InfiniteRunnerPlayerController : MonoBehaviour {

    public float movespeed;
    private float tempmovespeed = 0;
    private float activemovespeed;
    public bool canmove;
    public float speedMultiplayer;
    public float speedIncreaseMilestone;
    private float speedMileStoneCount;

    public Rigidbody2D myRigidbody;
    public float jumpspeed;

    public Transform firepoint;
    public GameObject fireball;

    public float meleeTimer;
    public float timeToMelee;
    public bool readyToMelee = false;

    public float shootTimer;
    public float timeToShoot;
    public bool readyToShoot = false;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;

    public bool isGrounded;
    public float vspeed;
    private Animator myAnim;

    public Vector2 respawnPosition;


    public LevelManager theLevelManager;

    public GameObject stompBox;

    public float knockbackForce;
    public float knockbackLength;
    private float knockbackCounter;

    public float invincibilityLength;
    private float invincibilityCounter;
    public AudioSource jumpsfx;
    public AudioSource hurtsfx;
    public AudioSource swordsound;

    private bool onPlatform;
    public float onPLatformSpeedModifier;

    public bool onLadder;
    public float climbSpeed;
    public float climbVelocity;
    public float gravityStore;

    public bool doubleJump;
    // Use this for initialization
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnim = GetComponent<Animator>();
        gravityStore = myRigidbody.gravityScale;
        respawnPosition = transform.position;

        theLevelManager = FindObjectOfType<LevelManager>();
        speedMileStoneCount = speedIncreaseMilestone;
        activemovespeed = movespeed;
        tempmovespeed = movespeed;
        canmove = true;
    }

    // Update is called once per frame
    void Update()
    {
        //if(!isLocalPlayer)
        //{
        //    return;
        //}

        // this code checks the milestone hit allong the inifinite runner an dif a milstone is hit the the speed of the player increases
        // and the increase will get bigger and bigger as the player continues making them go faster and faster
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        if (transform.position.x > speedMileStoneCount)
        {
            speedMileStoneCount += speedIncreaseMilestone;
            speedIncreaseMilestone += speedIncreaseMilestone * speedMultiplayer;
            movespeed = movespeed * speedMultiplayer;
        }
        if (knockbackCounter <= 0 && canmove)
        {
            

            if (onLadder)
            {
                myRigidbody.gravityScale = 0f;
                climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");

                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, climbVelocity);
            }
            if (!onLadder)
            {
                myRigidbody.gravityScale = gravityStore;
            }


            if (onPlatform)
            {
                activemovespeed = movespeed * onPLatformSpeedModifier;
            }
            else
            {
                activemovespeed = movespeed;
            }

            vspeed = myRigidbody.velocity.y;
            // the velocity of the rigidbody is set to a constant number on the x-axis making is run along the screen continually without the player
            // being able to alter the direction
            myRigidbody.velocity = new Vector2(activemovespeed, myRigidbody.velocity.y);
            transform.localScale = new Vector2(1f, 1f);

            if(isGrounded)
            {
                doubleJump = false;
            }
            // double jump and is grounded are check and if the player is has not used there double jump they can jump again
            if (Input.GetButtonDown("Jump") && (isGrounded || !doubleJump))
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpspeed);
                jumpsfx.Play();
                // is the player is in the air and not groundedtheir double jump will equal true
                if(!isGrounded)
                {
                    doubleJump = true;
                }
            }

            if (meleeTimer >= timeToMelee)
            {
                //movespeed = tempmovespeed;
                readyToMelee = true;

            }
            else
            {
                readyToMelee = false;
                meleeTimer += Time.deltaTime;
            }


            if (shootTimer >= timeToShoot)
            {
                readyToShoot = true;
            }
            else
            {
                readyToShoot = false;
                shootTimer += Time.deltaTime;
            }



            if (myAnim.GetBool("SpellCast"))
            {
                myAnim.SetBool("SpellCast", false);
            }

            if (myAnim.GetBool("HeavyAttack"))
            {
                myAnim.SetBool("HeavyAttack", false);

            }

            if (Input.GetKeyDown(KeyCode.K))
            {
                if (readyToMelee && isGrounded)
                {
                    myAnim.SetBool("HeavyAttack", true);
                    //movespeed -= movespeed;
                    swordsound.Play();
                    meleeTimer = 0;
                }

            }


            if (Input.GetKeyDown(KeyCode.F))
            {

                if (readyToShoot)
                {
                    myAnim.SetBool("SpellCast", true);
                    Instantiate(fireball, firepoint.position, firepoint.rotation);
                    shootTimer = 0;
                }
            }
            theLevelManager.invincible = false;
        }

        if (knockbackCounter > 0)
        {
            knockbackCounter -= Time.deltaTime;
            if (transform.localScale.x > 0)
            {
                myRigidbody.velocity = new Vector2(-knockbackForce, knockbackForce);
            }
            else
            {
                myRigidbody.velocity = new Vector2(knockbackForce, knockbackForce);
            }
        }
        if (invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;
        }
        if (invincibilityCounter <= 0)
        {
            theLevelManager.invincible = false;
        }
        myAnim.SetFloat("VerticalSpeed", myRigidbody.velocity.y);
        myAnim.SetFloat("Speed", Mathf.Abs(myRigidbody.velocity.x));
        myAnim.SetBool("Grounded", isGrounded);

        if (myRigidbody.velocity.y < 0)
        {
            stompBox.SetActive(true);
        }
        else
        {
            stompBox.SetActive(false);
        }
    }

    public void Knockback()
    {
        knockbackCounter = knockbackLength;
        invincibilityCounter = invincibilityLength;
        theLevelManager.invincible = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "KillZone")
        {
            //gameObject.SetActive(false);
            //transform.position = respawnPosition;
            theLevelManager.Respawn();
        }
        if (collision.tag == "Checkpoint")
        {
            respawnPosition = collision.transform.position;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "MovingPlatform")
        {
            transform.parent = collision.transform;
            onPlatform = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "MovingPlatform")
        {
            transform.parent = null;
            onPlatform = false;
        }
    }
}
//[GAVIN END]