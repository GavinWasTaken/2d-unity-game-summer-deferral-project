﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using UnityEngine;
using System.Collections;
// this script is used with enemy to make them travel back and forth between two points
public class PatrolPoints : MonoBehaviour {

    // gameobject for the enemy
    public GameObject enemy;

    // the enemys move speed
    public float moveSpeed;

    // the current point of the patrolling gameobject
    public Transform currentPoint;

    // an array of the points to travel between
    public Transform[] points;


    public int pointSelection;
    // Use this for initialization
    void Start()
    {
        // sets the currentpoint to one of the points the gameobject travels to
        currentPoint = points[pointSelection];
    }

    // Update is called once per frame
    void Update()
    {
        //the eneemys position is set to mov towards the current point at the moving speed
        enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, currentPoint.position, Time.deltaTime * moveSpeed);

        // if the enemy's position is the currentpoint position
        if (enemy.transform.position == currentPoint.position)
        {
            // point selection is increased by 1
            pointSelection++;
            //if the pointselection is the size of the array
            if (pointSelection == points.Length)
            {
                // the pointselection is reset to 0
                pointSelection = 0;
            }
            // the current point is reset to a point in the points array
            currentPoint = points[pointSelection];
        }
    }
}
//[GAVIN END]