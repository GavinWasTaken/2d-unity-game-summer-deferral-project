﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//This script is using for collectibles objects to help add score to each collectible based on what collectible it is.
public class Coin : MonoBehaviour {

    //creates an instance of the levelmanager script
    private LevelManager theLevelManager;

    // sets the objects score value
    public int coinValue;
	// Use this for initialization
	void Start () {
        // sets the instance to the levelmanager within the scene
        theLevelManager = FindObjectOfType<LevelManager>();
	}


    // when a gameobjects collided enters into another gameobjects collider
    void OnTriggerEnter2D(Collider2D collision)
    {
        // check if the colliding gameobjects has a tag of 'Player'
        if(collision.tag == "Player")
        {
            // activates the addcoins functions from the level manager using the scripts interger 'coinvalue'
            theLevelManager.AddCoins(coinValue);
            // sets the gameobject to inactive upon its pick up
            gameObject.SetActive(false);
        }
    }
}
//[GAVIN END]