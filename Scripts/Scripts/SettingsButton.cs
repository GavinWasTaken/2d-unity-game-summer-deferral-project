﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// script used with the main menu settings button to bring up text on screen 
// by setting different texts to active and inactive
public class SettingsButton : MonoBehaviour {

    // the two different texts to be set
    public GameObject settings;
    public GameObject tips;
	// Use this for initialization

    // used with the opensettings button when clicked the tips text
    // will be set to inactive and the setting set to active
    public void opensettings()
    {
        tips.SetActive(false);
        settings.SetActive(true);
    }
    // used with the tips button when clicked the tips text
    // will be set to active and the setting set to inactive
    public void opentips()
    {
        settings.SetActive(false);
        tips.SetActive(true);
        
    }
}
//[GAVIN END]