﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplode : MonoBehaviour {

    //We reference the animator so that we can code functions to do with this gameobjects animator and animations.
    public Animator animation;

    //The rigidbody2d is an object set here in order to use physics with this game object. 
    //This is very useful when you want the object to have a velocity or gravity or other physics attributes
    public Rigidbody2D myrb;
    
    //creating an instance of the enemyhealthmanager script
    private EnemyHealthManager ehm;
    
    //creating an instance of the levelmanager script
    public LevelManager theLevelManager;

    // a delay given 
    public float delay;
    // Use this for initialization

    void Start () {

        // The code finds an object within the games hierarchy that has a level manager script attached. Since there is only one level manager object per game scene we onl need to search for one object. This is then assigned to the variable 'theLevelManager'
        theLevelManager = FindObjectOfType<LevelManager>();
        
        // gets the animations from the animator component on this gameobject
        animation = GetComponent<Animator>();

        // gets the rigidbody2d from a component of this gameobject
        myrb = GetComponent<Rigidbody2D>();

        //at the beginning we set the parameter used within the animator to false so the animation will not start straight away
        animation.SetBool("Bomb", false);
    }

    // When an incoming collider makes contact with this object's collider no matter if it is set to trigger or not
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // the animators bomb parameter is set to true letting its animation take place
        animation.SetBool("Bomb", true);

        // its rigidbody2d's velocity of set to zero along both its x-axis and y-axis
        myrb.velocity = Vector2.zero;

        // the scripts gameobject it destroyedafter its delay is complete letting the animation play before the object is destroyed.
        // when the object is destroyed it is removed completely from the game scene
        Destroy(gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delay);
        
    }
}
// [GAVIN END]