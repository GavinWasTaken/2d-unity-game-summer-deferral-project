﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//this is a script to be used with the boss enemy that is present in the last level of the main game

public class BossHandHealthManager : MonoBehaviour {
    
    //the bosses main health
    public int maxHealth;
    
    //the bosses health
    public int enemyHealth;

    // the gameobject to be set which will be used to give its deathparticle effect
    public GameObject enemyDeathParticle;

    //the items that it can drop
    public GameObject[] dropItems;

    //The rigidbody2d is an object set here in order to use physics with this game object. 
    //This is very useful when you want the object to have a velocity or gravity or other physics attributes
    public Rigidbody2D enemyRigidbody;

    //a knockback given to the player when touching off the boss
    public float knockbackforceY;

    // The sound effect to be player with the boss is hit
    public AudioSource hit;

    // a gameobject which is the eaxt copy of the boss
    public GameObject bossprefab;

    // the minimum size the boss can spilt into
    public float minsize;
    

    // Use this for initialization
    void Start()
    {
        // setting its rigidbody to that of the rigidbody attach to its gameobject
        enemyRigidbody = GetComponent<Rigidbody2D>();

        // setting the bosses health to its max amount of health
        enemyHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {

        // if the bosses health reach below or equal to 0
        if (enemyHealth <= 0)
        {

            // the gameobject instance of the bosses death particle is spawned into the game at the smae position and rotation of the boss
            Instantiate(enemyDeathParticle, transform.position, transform.rotation);

            //this current gameobject is set to inactive withing the screen
            gameObject.SetActive(false);

            // arandom item from the drop items array of items and then spawned at the bosses position and rotation
            Instantiate(dropItems[Random.Range(0, dropItems.Length)], transform.position, Quaternion.identity);

            // check to see if its localscale is still more than the minumum size if so it carries on
            if(transform.localScale.y > minsize)
            {

                // spawns in two instance of the gameobject of the boss one on the right and one on the left of the previouses bosses position as gameobjects
                GameObject clone1 = Instantiate(bossprefab, new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z), transform.rotation) as GameObject;
                GameObject clone2 = Instantiate(bossprefab, new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z),transform.rotation) as GameObject;


                //eahc one of these newly spawned instance have thse scales divided in half and thier health set to 10
                clone1.transform.localScale = new Vector3(transform.localScale.x * 0.5f,transform.localScale.y * 0.5f, transform.localScale.z);
                clone1.GetComponent<BossHandHealthManager>().enemyHealth = 10;

                clone2.transform.localScale = new Vector3(transform.localScale.x * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z);
                clone2.GetComponent<BossHandHealthManager>().enemyHealth = 10;
            }

        }
    }
    //public void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.transform.tag == "Projectile")
    //    {
    //        var contact = collision.contacts[0];
    //        Debug.Log("hit something" + contact);
    //    }
    //        
    //}

    // Damage to be applyed to the boss which subtracted from its curent health applying 
    // a small knockback effect and then playing the sound effect of the boss being damaged
    public void ApplyDamage(int damageGiven)
    {
        enemyHealth -= damageGiven;
        enemyRigidbody.velocity = new Vector2(0, knockbackforceY);
        hit.Play();
    }
}
// [GAVIN END]