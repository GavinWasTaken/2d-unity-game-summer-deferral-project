﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour {
    public int enemyHealth;
    public GameObject enemyDeathParticle;
    public GameObject[] dropItems;
    public Rigidbody2D enemyRigidbody;
    public float knockbackforceY;
    public AudioSource hit;
	// Use this for initialization
	void Start () {
        enemyRigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(enemyHealth <=0)
        {
            Instantiate(enemyDeathParticle, transform.position, transform.rotation);
            gameObject.SetActive(false);
            Instantiate(dropItems[Random.Range(0, dropItems.Length)], transform.position, Quaternion.identity);
            float counter = Time.deltaTime;
        }
	}
    //public void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.transform.tag == "Projectile")
    //    {
    //        var contact = collision.contacts[0];
    //        Debug.Log("hit something" + contact);
    //    }
    //        
    //}

    public void ApplyDamage(int damageGiven)
    {
        enemyHealth -= damageGiven;     
        enemyRigidbody.velocity = new Vector2(0, knockbackforceY);
        hit.Play();
    }
}
