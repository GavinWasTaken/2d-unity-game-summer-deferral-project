﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is used within the inifinite runner game to set all objects that are behind it on the x-axis to inactive.
public class Destroyer : MonoBehaviour {
    // set the point to which any object that is behind will be set to inactive
    public GameObject destroyerPoint;
    private void Start()
    {
        //the detroypoint will be the object within the scene that has the name 'Destroyer
        destroyerPoint = GameObject.Find("Destroyer");
    }
    private void Update()
    {
        // This is the continued check for the object that are behind the destroy point that will be set as inactive
        if(transform.position.x < destroyerPoint.transform.position.x)
        {
            gameObject.SetActive(false);
        }
    }

}
//[GAVIN END]