﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballController : MonoBehaviour {
    public float speed;
    public Rigidbody2D myrigidbody;
    public GameObject enemyDeathParticle;
    public GameObject impactParticle;
    public int damageToGive;
    private PlayerMovement thePlayer;
    // Use this for initialization
    void Start () {
        myrigidbody = GetComponent<Rigidbody2D>();
        thePlayer = FindObjectOfType<PlayerMovement>();
        
        if(thePlayer.transform.localScale.x < 0)
        {
            speed = -speed;
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        }

        if (Input.GetKey(KeyCode.W))
        {
            myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, speed, transform.rotation.z * 90);
            gameObject.transform.Rotate(0, 0, 90);
            if(thePlayer.transform.localScale.x < 0)
            {
                myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, -speed, transform.rotation.z * 90);
                gameObject.transform.Rotate(0, 0, 180);
            }
        }
        else if (Input.GetKey(KeyCode.S))
        {
            myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, -speed, transform.rotation.z * 90);
            gameObject.transform.Rotate(0, 0, -90);
            if (thePlayer.transform.localScale.x < 0)
            {
                myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, speed, transform.rotation.z * 90);
                gameObject.transform.Rotate(0, 0, -180);
            }
        }
        else
        {
            myrigidbody.velocity = new Vector2(speed, myrigidbody.velocity.y);
        }
    }

    // Update is called once per frame
    void Update () {
        //myrigidbody.velocity = new Vector2(speed, myrigidbody.velocity.y);
        
        //if(Input.GetKey(KeyCode.W))
        //{
        //    myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, speed, transform.rotation.z * 90);
        //}
        //else
        //{
            //myrigidbody.velocity = new Vector2(speed, myrigidbody.velocity.y);
        //}
        
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Collider2D collider = collision.collider;
    //    
    //    if (collider.name == "Light Warrior")
    //    {
    //        Vector3 dir = collision.contacts[0].point - gameObject.transform.position;
    //        Vector3 contactPoint = collision.contacts[0].point;
    //        Vector3 center = collider.bounds.center;
    //
    //        if (contactPoint.x > center.x)
    //        {
    //            //GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
    //            collision.collider.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive, -knockback);
    //            Debug.Log("right");
    //        }
    //        else if (contactPoint.x < center.x)
    //        {
    //            collision.collider.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive, knockback);
    //            Debug.Log("left");
    //        }
    //        Instantiate(impactParticle, transform.position, transform.rotation);
    //
    //        Destroy(gameObject);
    //    }
    //}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
        }
        Instantiate(impactParticle, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
