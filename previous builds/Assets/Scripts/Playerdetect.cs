﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playerdetect : MonoBehaviour {
    private Animator animation;
    public Rigidbody2D myrb;
	// Use this for initialization
	void Start () {
        animation = GetComponent<Animator>();
        myrb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            myrb.velocity = new Vector2(0,0);
            animation.SetBool("attack", true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            animation.SetBool("attack", false);
        }
    }
}
