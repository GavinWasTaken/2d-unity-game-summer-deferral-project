﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectandbombplayer : MonoBehaviour {
    private Animator animation;
    public Rigidbody2D myrb;
    public GameObject bomb;
    public Transform bombPoint;
    public float animationTime;
    // Use this for initialization
    void Start()
    {
        animation = GetComponent<Animator>();
        myrb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Vector3 v = myrb.velocity;
            v.y = 0.0f;
            v.x = 0.0f;
            myrb.velocity = v;


            animation.SetBool("attack", true);
            Invoke("spawnBomb", animationTime);
            //Instantiate(bomb, bombPoint.position, bombPoint.rotation);
        }
       
    }
    public void spawnBomb()
    {
        
        GameObject b = (GameObject)Instantiate(bomb, bombPoint.position, bombPoint.rotation);
        b.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
}
