﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// script that uses a player pref that carries the logged in username across different scens to set the welcome message on the 
// main menu reconfirming the user they are logged in
public class WelcomeMessage : MonoBehaviour {
    // the text of the welcome message
    public Text message;
	// Update is called once per frame
	void Update () {
        // sets the message setting the player pref
        message.text ="Welcome, " + PlayerPrefs.GetString("UserName") + "!";
	}
}
//[GAVIN END]