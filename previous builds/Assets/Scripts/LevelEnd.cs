﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class LevelEnd : MonoBehaviour {
    public string levelToLoad;
    public static int score;
    private PlayerMovement thePlayer;
    private CameraController theCamera;
    private LevelManager theLevelManager;
    public GameObject endScore;
    public Text endScoreText;
    public float endScoreWaitTime;
    public float waitTillMove;
    public float waitToLoadLevel;

    private bool movePlayer;
	// Use this for initialization
	void Start () {
        thePlayer = FindObjectOfType<PlayerMovement>();
        theCamera = FindObjectOfType<CameraController>();
        theLevelManager = FindObjectOfType<LevelManager>();
        //theLevelManager = GetComponent<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if(movePlayer)
        {
            
            thePlayer.myRigidbody.velocity = new Vector2(thePlayer.movespeed, thePlayer.myRigidbody.velocity.y);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            StartCoroutine("LevelEndCo");
            //SceneManager.LoadScene(levelToLoad);
        }
    }
    public IEnumerator LevelEndCo()
    {
        
        thePlayer.canmove = false;
        theCamera.followingtarget = false;
        theLevelManager.invincible = true;
        PlayerPrefs.SetInt("CoinCount", theLevelManager.coinCount);

        PlayerPrefs.SetInt("PlayerLives", theLevelManager.currentLives);

        PlayerPrefs.SetInt("Score", theLevelManager.score);

        PlayerPrefs.SetInt("ScoreText", theLevelManager.score);

        theLevelManager.mainGameMusic.Stop();
        theLevelManager.levelcompleteMusic.Play();
        thePlayer.myRigidbody.velocity = Vector3.zero;
        
        yield return new WaitForSeconds(waitTillMove);
        movePlayer = true;
        yield return new WaitForSeconds(waitToLoadLevel);
        thePlayer.gameObject.SetActive(false);
        endScoreText.text = "Final Score: " + theLevelManager.score;
        endScore.SetActive(true);
        yield return new WaitForSeconds(endScoreWaitTime);
        endScore.SetActive(false);
        thePlayer.gameObject.SetActive(true);
        SceneManager.LoadScene(levelToLoad);

    }
}
