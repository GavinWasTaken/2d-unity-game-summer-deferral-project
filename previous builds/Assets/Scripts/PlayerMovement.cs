﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerMovement : NetworkBehaviour {

    public float movespeed;
    private float activemovespeed;
    public bool canmove;

    public Rigidbody2D myRigidbody;
    public float jumpspeed;

    public Transform firepoint;
    public GameObject fireball;
    public float shootTimer;
    public float timeToShoot;
    public bool readyToShoot = false;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;

    public bool isGrounded;
    public float vspeed;
    private Animator myAnim;

    public Vector2 respawnPosition;


    public LevelManager theLevelManager;

    public GameObject stompBox;

    public float knockbackForce;
    public float knockbackLength;
    private float knockbackCounter;

    public float invincibilityLength;
    private float invincibilityCounter;
    public AudioSource jumpsfx;
    public AudioSource hurtsfx;

    private bool onPlatform;
    public float onPLatformSpeedModifier;


    // Use this for initialization
    void Start () {
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnim = GetComponent<Animator>();
        respawnPosition = transform.position;

        theLevelManager = FindObjectOfType<LevelManager>();

        activemovespeed = movespeed;

        canmove = true;
	}
	
	// Update is called once per frame
	void Update () {
        //if(!isLocalPlayer)
        //{
        //    return;
        //}
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        if (knockbackCounter <= 0 && canmove)
        {
            if(onPlatform)
            {
                activemovespeed = movespeed * onPLatformSpeedModifier;
            }
            else
            {
                activemovespeed = movespeed;
            }

            vspeed = myRigidbody.velocity.y;

            if (Input.GetAxisRaw("Horizontal") > 0f)
            {
                myRigidbody.velocity = new Vector2(activemovespeed, myRigidbody.velocity.y);
                transform.localScale = new Vector2(1f, 1f);
            }
            else if (Input.GetAxisRaw("Horizontal") < 0f)
            {
                myRigidbody.velocity = new Vector2(-activemovespeed, myRigidbody.velocity.y);
                transform.localScale = new Vector2(-1f, 1f);
            }
            else
            {
                myRigidbody.velocity = new Vector2(0f, myRigidbody.velocity.y);
            }

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpspeed);
                jumpsfx.Play();

            }
            if(shootTimer >= timeToShoot)
            {
                readyToShoot = true;
            }
            else
            {
                readyToShoot = false;
                shootTimer += Time.deltaTime;
            }
            if(myAnim.GetBool("SpellCast"))
            {
                myAnim.SetBool("SpellCast", false);
            }
            //if(myAnim.GetBool("Heavy Attack"))
            //{
            //    myAnim.SetBool("Heavy Attack", false);
            //}
            if (Input.GetKeyDown(KeyCode.F) )
            {
                
                if (readyToShoot)
                {
                    myAnim.SetBool("SpellCast", true);
                    Instantiate(fireball, firepoint.position, firepoint.rotation);
                  shootTimer = 0;
                }
            }
            theLevelManager.invincible = false;
        }

        if(knockbackCounter > 0)
        {
            knockbackCounter -= Time.deltaTime;
            if(transform.localScale.x >0)
            {
                myRigidbody.velocity = new Vector2(-knockbackForce, knockbackForce);
            }
            else
            {
                myRigidbody.velocity = new Vector2(knockbackForce, knockbackForce);
            }
        }
        if(invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;
        }
        if(invincibilityCounter <=0)
        {
            theLevelManager.invincible = false;
        }
        myAnim.SetFloat("VerticalSpeed", myRigidbody.velocity.y);
        myAnim.SetFloat("Speed", Mathf.Abs(myRigidbody.velocity.x));
        myAnim.SetBool("Grounded", isGrounded);

        if(myRigidbody.velocity.y < 0)
        {
            stompBox.SetActive(true);
        }
        else
        {
            stompBox.SetActive(false);
        }
	}


    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }
    public void Knockback()
    {
        knockbackCounter = knockbackLength;
        invincibilityCounter = invincibilityLength;
        theLevelManager.invincible = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "KillZone")
        {
            //gameObject.SetActive(false);
            //transform.position = respawnPosition;
            theLevelManager.Respawn();
        }
        if(collision.tag == "Checkpoint")
        {
            respawnPosition = collision.transform.position;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "MovingPlatform")
        {
            transform.parent = collision.transform;
            onPlatform = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "MovingPlatform")
        {
            transform.parent = null;
            onPlatform = false;
        }
    }
}
