﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is used at the end of the main game to let the player submit their score and return to the main menu
public class MainGameComplete : MonoBehaviour {
    // the end game sound 
    public AudioSource endgametune;

    // the end game screen
    public GameObject endscreen;

    //instance of the level manager
    public LevelManager theLevelManager;
    //instance of the player
    public PlayerMovement thePlayer;

	// Use this for initialization
	void Start () {
        //looks for objects within the scene that match the scripts being searched and sets it 
        //to these instance
        theLevelManager = FindObjectOfType<LevelManager>();
        thePlayer = FindObjectOfType<PlayerMovement>();

	}

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'PLayer'
        if (collision.tag == "Player")
        {
            //pauses the audio clip
            theLevelManager.mainGameMusic.Pause();

            //plays the end game tune
            endgametune.Play();

            // freezes time
            Time.timeScale = 0f;

            // activates the endgame screen
            endscreen.SetActive(true);

            // makes it so the player can no longer move
            thePlayer.canmove = false;
        }
    }
}
//[GAVIN END]