﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is used within our other game type known as the infinite runner.
//its purpose is to randomly generate coins on random platforms for the player to collect as they play
public class CoinGenerator : MonoBehaviour {
    // Creating an instance of thwe objectpooler script
    public InfiniteRunnerObjectPooler coinPool;
    // float for the distance between the generated coins
    public float distanceBetweenCoins;
    // this function is for the spawning of three coins at a given vector whihc will be used to position them
    public void SpawnCoins(Vector3 startPositiion)
    {
        // The first coin will be recieved from the pooler and set to the given vectors position
        // then set to active within the game
        GameObject coin1 = coinPool.GetPooledObject();
        coin1.transform.position = startPositiion;
        coin1.SetActive(true);

        // The second coins is set all the same except its x-axis position has the 
        // added distance set to make the coin stay beside the first coin
        GameObject coin2 = coinPool.GetPooledObject();
        coin2.transform.position = new Vector3(startPositiion.x - distanceBetweenCoins, startPositiion.y, startPositiion.z);
        coin2.SetActive(true);

        // The second coins is set all the same except its x-axis position has the 
        // distance subtracts to make the coin stay beside the first coin
        GameObject coin3 = coinPool.GetPooledObject();
        coin3.transform.position = new Vector3(startPositiion.x + distanceBetweenCoins, startPositiion.y, startPositiion.z);
        coin3.SetActive(true);
    }
}
//[GAVIN END]