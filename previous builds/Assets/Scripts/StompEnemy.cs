﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StompEnemy : MonoBehaviour {
    private Rigidbody2D playerrigidbody;
    public GameObject deathParticle;
    public float bounceForce;

	// Use this for initialization
	void Start () {
        playerrigidbody = transform.parent.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            //Destroy(collision.gameObject);
            collision.gameObject.SetActive(false);

            Instantiate(deathParticle, collision.transform.position, collision.transform.rotation);

            playerrigidbody.velocity = new Vector2(playerrigidbody.velocity.x, bounceForce);

        }
    }
}
