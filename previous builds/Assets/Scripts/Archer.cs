﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : MonoBehaviour {
    public Rigidbody2D myRigidbody;
    public PlayerMovement thePlayer;
    public Animator animation;
    public Transform firepoint;
    public float rundistance;
    public GameObject arrow;
    public float firedelay;
    public float firecounter;

    
    public float shootRange;
    public LayerMask playerLayer;
    public LayerMask shootLayer;
    
    public bool inShootRange;
    
    public bool isPlayerRight;
    // Use this for initialization
    void Start () {
        
        myRigidbody = GetComponent<Rigidbody2D>();
        thePlayer = FindObjectOfType<PlayerMovement>();
        animation = GetComponent<Animator>();

        firecounter = firedelay;
    }
	
	// Update is called once per frame
	void Update () {
        firecounter -= Time.deltaTime;
        if (isPlayerRight  && thePlayer.transform.position.x < transform.position.x)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            isPlayerRight = false;
        }
        if (!isPlayerRight  && thePlayer.transform.position.x > transform.position.x)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            isPlayerRight = true;
        }

        
        inShootRange = Physics2D.OverlapCircle(transform.position, shootRange, shootLayer);
        if (inShootRange && firecounter <= 0)
        {
            //inRange = false;

            //myRigidbody.velocity = Vector2.zero;
            //myRigidbody.velocity = Vector2.zero;
            //myRigidbody.Sleep();
            //transform.position = Vector2.MoveTowards(transform.position, thePlayer.transform.position, );
            animation.Play("attack");
            
            
            firecounter = firedelay;
        }

        
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, shootRange);
    }

    void spawnArrow()
    {
        Instantiate(arrow, firepoint.position, firepoint.rotation);
    }
}
