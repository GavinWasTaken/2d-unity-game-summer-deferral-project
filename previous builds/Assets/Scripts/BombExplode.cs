﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplode : MonoBehaviour {
    public Animator animation;
    public Rigidbody2D myrb;
    private EnemyHealthManager ehm;
    public LevelManager theLevelManager;
    public int damageToGive;
    public float delay;
    // Use this for initialization
    void Start () {
        theLevelManager = FindObjectOfType<LevelManager>();
        animation = GetComponent<Animator>();
        myrb = GetComponent<Rigidbody2D>();
        ehm = GetComponent<EnemyHealthManager>();
        animation.SetBool("Bomb", false);
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        //animation.Play("Bomb");
        animation.SetBool("Bomb", true);
        myrb.velocity = Vector2.zero;
        Destroy(gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delay);
        
        //gameObject.SetActive(false);
    }
    

   //private void OnTriggerEnter2D(Collider2D collision)
   //{
   //    if(collision.tag == "Ground")
   //    {
   //        animation.SetBool("Bomb", true);
   //        //theLevelManager.hurtPlayer(damageToGive);
   //        Destroy(gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delay);
   //    }
   //    if (collision.tag == "Player")
   //    {
   //        
   //        animation.SetBool("Bomb", true);
   //        theLevelManager.hurtPlayer(damageToGive);
   //        Destroy(gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delay);
   //    }
   //
   //    //
   //    //collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
   //    //else if (collision.tag == "Enemy")
   //    //{
   //    //    animation.GetBool("Bomb");
   //    //    animation.SetBool("Bomb", true);
   //    //    collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
   //    //}
   //    //else if (collision.tag == "Ground")
   //    //{
   //    //    animation.GetBool("Bomb");
   //    //    animation.SetBool("Bomb", true);
   //    //    collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
   //    //}
   //    //else if (collision.tag == "MovingPlatform")
   //    //{
   //    //    animation.GetBool("Bomb");
   //    //    animation.SetBool("Bomb", true);
   //    //    collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
   //    //}
   //    //else if (collision.tag == "EnviromentalDanger")
   //    //{
   //    //    animation.GetBool("Bomb");
   //    //    animation.SetBool("Bomb", true);
   //    //    collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
   //    //}
   //    //else if (collision.tag == "Projectile")
   //    //{
   //    //    animation.GetBool("Bomb");
   //    //    animation.SetBool("Bomb", true);
   //    //    collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
   //    //}
   //    //Destroy(gameObject);
   //}
    //private void OnTriggerExit2D(Collider2D collision)
    //{
    //    if (collision.tag == "Player" || collision.tag == "Enemy")
    //    {
    //        animation.SetBool("Bomb", false);
    //    }
    //}
}
