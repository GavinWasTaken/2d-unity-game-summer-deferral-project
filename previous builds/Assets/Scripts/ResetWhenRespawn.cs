﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetWhenRespawn : MonoBehaviour {

    private Vector2 startPos;
    private Quaternion startRot;
    private Vector2 startLocalScale;
    private Rigidbody2D myrigidbody;
	// Use this for initialization
	void Start () {
        
        startPos = transform.position;
        startRot = transform.rotation;
        startLocalScale = transform.localScale;

        if (GetComponent<Rigidbody2D>() != null)
        {
            myrigidbody = GetComponent<Rigidbody2D>();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void reset()
    {
        
        transform.position = startPos;
        transform.rotation = startRot;
        transform.localScale = startLocalScale;

        if(myrigidbody != null)
        {
            myrigidbody.velocity = Vector2.zero;
        }
    }
}
