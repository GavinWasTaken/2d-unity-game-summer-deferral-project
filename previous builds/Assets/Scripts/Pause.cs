﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Pause : MonoBehaviour {
    public string mainmenu;
    private LevelManager theLevelManager;
    public GameObject pauseScreen;
    private PlayerMovement thePlayer;
	// Use this for initialization
	void Start () {
        theLevelManager = FindObjectOfType<LevelManager>();
        thePlayer = FindObjectOfType<PlayerMovement>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(Time.timeScale == 0f)
            {
                resumeGame();
            }
            else
            {
                pauseGame();
            }
        }
    }
    public void pauseGame()
    {
        Time.timeScale = 0f;
        pauseScreen.SetActive(true);

        thePlayer.canmove = false;

        theLevelManager.mainGameMusic.Pause();
    }

    public void resumeGame()
    {
        pauseScreen.SetActive(false);
        Time.timeScale = 1f;
        thePlayer.canmove = true;
        theLevelManager.mainGameMusic.Play();
    }
    public void quitToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainmenu);
    }
}
