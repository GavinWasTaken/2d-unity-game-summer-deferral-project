﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// This script will be used to spawn in the different platforms. Using the object pooler to recycle the platforms 
// and reset there positions
public class InfiniteRunnerSpawner : MonoBehaviour {
    //an array of the pooled objects
    public InfiniteRunnerObjectPooler[] theObjectPools;
    
    //instance of the coin Gernerator
    public CoinGenerator theCoinGenerator;

    // Transforms of the gerneration point
    public Transform generationPoint;

    // the distance between each platform
    public float distanceBetween;

    // the minimum distance between each platform
    public float distanceMin;

    // the maximum distance between each platform
    public float distanceMax;

    //each platforms widths
    private float platformWidth;

    // integer for platform selecting
    private int platformSelector;

    //an array of the platform widths
    private float[] platformwidths;
    
    // the minimum height for the platofrm to go to
    private float minHeight;

    // the point of an object used to set the maximum height
    public Transform maxHeightPoint;

    // the maximum heigh for the platforms to go to
    private float maxHeight;

    //if the height is to change this will be the maximum it can change to
    public float maxHeightChange;

    // the change of the hieght a platform will make
    private float heightChange;

    // the random float used to help with spawning in a coin on the platforms
    public float randomCoinThreshold;
	// Use this for initialization
	void Start () {
        // initialising the coinerator to an object within the scene that has the CoinGenerator Script
        theCoinGenerator = FindObjectOfType<CoinGenerator> ();

        // initialising the platform widths to a new float of the objectpoolarray's length
        platformwidths = new float[theObjectPools.Length];

        // this loop goes through all the object within pooled objects getting the size of their 2d boxcolliders alon their x-axis
        for(int i = 0; i < theObjectPools.Length; i ++)
        {
            platformwidths[i] = theObjectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
        }

        // the minimum height is equal to the starting y position
        minHeight = transform.position.y;

        // the mazhieght is equal to the maxheightPount within the game scene
        maxHeight = maxHeightPoint.position.y;
        //Spawn();
	}
	
	// Update is called once per frame
	void Update () {
        // if the current point is less than the gerneration point on the x axis
        if(transform.position.x < generationPoint.transform.position.x)
        {
            // a random distance between the minimum distance and maximum distance 
            // is now the distancebetween the current platform and the next platform
            distanceBetween = Random.Range(distanceMin, distanceMax);
            
            // this makes the platform that is selected to be used equal to a random pooled object within the array of pooled objects
            platformSelector = Random.Range(0, theObjectPools.Length);

            //the height change will be equals to the current y position plus a 
            //random value between the maxheightchange value and the negative of the maxheightchange value
            heightChange = transform.position.y + Random.Range(maxHeightChange, -maxHeightChange);

            // if the heightchange is more than the maxheight then the height change will only equal to the
            // maxheight
            if(heightChange > maxHeight)
            {
                heightChange = maxHeight;
            }
            // if the heightchange is less than the minheight then the heightchange is equal to the minheight
            else if (heightChange < minHeight)
            {
                heightChange = minHeight;
            }

            // the position of the game object is now changed another position 
            // made up with the between distance and the wif=dthes of the  platform selector
            transform.position = new Vector3((transform.position.x + platformwidths[platformSelector] /2)+ distanceBetween, heightChange, transform.position.z);

            // the new platform is now equal to one of the platforms form the pooled objects
            GameObject newPlatform = theObjectPools[platformSelector].GetPooledObject();
            // its= then sets its position and rotation and then makes itself active
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);

            // this part uses a random values between 0 and 100 and if that value is < the random coin threshold then
            // coins are spawned onto one of the platforms
            if(Random.Range(0f, 100f) < randomCoinThreshold)
            {
                theCoinGenerator.SpawnCoins(new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z));
            }

            // the game objects position is then resetwithout the distanccebetween being used
            transform.position = new Vector3(transform.position.x + platformwidths[platformSelector] /2, transform.position.y, transform.position.z);
        }
	}

}
//[GAVIN END]