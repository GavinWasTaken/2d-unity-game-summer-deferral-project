﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FadeOut : MonoBehaviour {
    public float time;
    private Image blackscreen;
	// Use this for initialization
	void Start () {
        blackscreen = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        blackscreen.CrossFadeAlpha(0f, time, false);

        if(blackscreen.color.a == 0)
        {
            gameObject.SetActive(false);
        }
	}
}
