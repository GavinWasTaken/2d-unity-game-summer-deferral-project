﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//this script is used to insert new score into the online database from the player within the unity game
public class InsertPlayerScore : MonoBehaviour {

    // variable used here are the playersname, the score they have amounted, the gametype. 
    // an instance of the player manager
    // the url to be used for the online php script
    // succcess and failed messages
    // and the bool for checking whether the score was added
    public string PlayerName;
    public int PlayerScore;
    public string GameType;
    public LevelManager theLevelManager;

    public GameObject successMessage;
    public GameObject failMessage;
    public float messageTime;
    private string createPlayerScoreUrl = "https://gavinhickeyty4.000webhostapp.com/InsertScore.php";
    public bool wasScoreAdded;
	// Use this for initialization
	void Start () {
        theLevelManager = FindObjectOfType<LevelManager>();
	}
    // this function uses the player pref of the username, the current score 
    // from the level manager with the coroutine to add them to the database
    public void createscore()
    {
        PlayerName = PlayerPrefs.GetString("UserName");
        PlayerScore = theLevelManager.score;
        
        StartCoroutine(createPlayerScore(PlayerName, PlayerScore, GameType));
    }

    // this coroutine uses information to submit a playerscore to the database
    IEnumerator createPlayerScore(string playername, int playerscore, string gametype)
    {
        // check to see that the submitted score has a playername and if it doesn't then only a fail messagee is given
        if (PlayerName == string.Empty || PlayerName == " ")
        {
            wasScoreAdded = false;
            failMessage.SetActive(true);
            yield return new WaitForSeconds(messageTime);
            failMessage.SetActive(false);            
        }

        // a www form is useed to enter in the data variable needed for a submit
        else
        {
            WWWForm form = new WWWForm();
            form.AddField("playernamePost", playername);
            form.AddField("playerscorePost", playerscore);
            form.AddField("gametypePost", gametype);

            // using a www class and a url to the php script the submit is sent
            WWW www = new WWW(createPlayerScoreUrl, form);
            yield return www;
            Debug.Log(www.text);

            // the php script returns and specific message if the submit was successful.
            // If the response contains this message then we know that the submit was successful and
            // we can tell the player it was added by showing the success message
            if (www.text.Contains("score added"))
            {
                wasScoreAdded = true;
                successMessage.SetActive(true);
                yield return new WaitForSeconds(messageTime);
                successMessage.SetActive(false);
            }
            // otherwise the submit is not successful and the player is given the fail message
            else
            {
                wasScoreAdded = false;
                failMessage.SetActive(true);
                yield return new WaitForSeconds(messageTime);
                failMessage.SetActive(false);
            }
        }
    }
}
//[GAVIN END]