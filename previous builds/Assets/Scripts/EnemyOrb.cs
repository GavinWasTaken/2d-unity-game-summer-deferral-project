﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOrb : MonoBehaviour {
    public Rigidbody2D myrigidbody;
    public GameObject impactParticle;
    public int damageToGive;
    public float speed;
    private PlayerMovement thePlayer;
    private LevelManager theLevelManager;
    // Use this for initialization
    void Start () {
        myrigidbody = GetComponent<Rigidbody2D>();
        theLevelManager = FindObjectOfType<LevelManager>();
        thePlayer = FindObjectOfType<PlayerMovement>();
        //myrigidbody.velocity = new Vector2(speed, -speed);
    }
    private void FixedUpdate()
    {
        this.transform.position = Vector2.MoveTowards(transform.position, new Vector2(thePlayer.transform.position.x, 0), speed * Time.deltaTime);
    }
    // Update is called once per frame
    void Update () {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Player")
        {
            theLevelManager.hurtPlayer(damageToGive);

        }
        Instantiate(impactParticle, transform.position, transform.rotation);
        Destroy(gameObject);
    }
    //public void LookAt2D(this Transform transform, Vector2 target)
    //{
    //    Vector2 current = transform.position;
    //    var direction = target - current;
    //    var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
    //    transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    //}
}
