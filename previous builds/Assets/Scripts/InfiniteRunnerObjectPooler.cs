﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is used to help set up a list of pooled object for the infinite runner game.
// These pooled objects will be used to recyled game objects instead of estroying them which make the game run much more efficiently
public class InfiniteRunnerObjectPooler : MonoBehaviour {
    // game object to pool
    public GameObject pooledObject;

    // the amount of pooled game objects 
    public int pooledAmount;

    // A list of pooled game objects 
    List<GameObject> pooledObjects;

	// Use this for initialization
	void Start () {

        //making the pooledobjects equal to a list of gameobjects
        pooledObjects = new List<GameObject>();
        
        // a loop through the pooled amount which will make a game object equal to the spawned in pooled game objects
        // set them to active within the scene and then add this object to the list
        for(int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(pooledObject);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }	
	}
	
    // a method used to retrieve all the pooled game objects
    public GameObject GetPooledObject()
    {
        // lopps through the count of all the pooled objects
        for(int i = 0; i < pooledObjects.Count; i++)
        {
            //checks for the active objects within the scene and then returns them
            if(!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }

        // creates a game object equal to the spawned in pooled game objects
        // set them to active within the scene and then add this object to the list

        GameObject obj = (GameObject)Instantiate(pooledObject);
        obj.SetActive(false);
        pooledObjects.Add(obj);
        return obj;
    }
}

//[GAVIN END]