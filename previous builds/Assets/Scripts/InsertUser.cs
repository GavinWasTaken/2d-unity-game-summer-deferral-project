﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsertUser : MonoBehaviour {
    public string inputname;
    public string inputemail;
    public string inputpassword;
    string createuserUrl = "https://gavinhickeyty4.000webhostapp.com/InsertUser.php";
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(CreateUser(inputname, inputemail, inputpassword));
        }
	}

    IEnumerator CreateUser(string username, string email, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("emailPost", email);
        form.AddField("passwordPost", password);

        WWW www = new WWW(createuserUrl, form);
        yield return www;
        Debug.Log(www.text);
    }
}
