﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour {
    public string newGame;
    public string continueGame;
    public int startingLives;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void NewGame()
    {
        SceneManager.LoadScene(newGame);
        PlayerPrefs.SetInt("CoinCount", 0);
        PlayerPrefs.SetInt("PlayerLives", startingLives);
        PlayerPrefs.SetInt("Score", 0);
    }
    public void Continue()
    {
        SceneManager.LoadScene(continueGame);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
