﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour {
    public GameObject enemyDeathParticle;
    
    public EnemyHealthManager ehm;
    public int damageToGive;
    // Use this for initialization
    void Start () {
        EnemyHealthManager ehm = GetComponent<EnemyHealthManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {

            Instantiate(enemyDeathParticle, collision.transform.position, collision.transform.rotation);
            Destroy(collision.gameObject);
        }
        
        
        //Destroy(gameObject);
    }
}
