﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkpointController : MonoBehaviour {
    public Sprite active;
    public Sprite inactive;

    public bool checkpointActive;
    private LevelManager theLevelManager;
    private SpriteRenderer thisSpriteRenderer;
    public int newCoinCount;
    public int newScore;
	// Use this for initialization
	void Start () {
        thisSpriteRenderer = GetComponent<SpriteRenderer>();
        theLevelManager = FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            thisSpriteRenderer.sprite = active;
            checkpointActive = true;
            newCoinCount = theLevelManager.coinCount;
            newScore = theLevelManager.score;
        }
    }
}
