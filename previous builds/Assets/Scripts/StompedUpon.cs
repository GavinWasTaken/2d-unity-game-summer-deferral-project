﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script implys damage to the emeny when hit by the player on their stompbox
// not all enemies will have a stomp box on them so not all can be jumped on
public class StompedUpon : MonoBehaviour {
    // gets an instance of the enemy healthmanager and the damge to give to the enemy
    public EnemyHealthManager ehm;
    public int damageToRecieve;
	// Use this for initialization
	void Start () {
        // sets the instance to the enemy health manager component attach to the gameobject
        ehm = GetComponentInParent<EnemyHealthManager>(); ;
	}

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'PLayer'
        if (collision.tag == "Player")
        {
            // the applydamage of the enemy health manager is used allong with the damage to send to the player
            ehm.ApplyDamage(damageToRecieve);
        }
    }
}
//[GAVIN END]