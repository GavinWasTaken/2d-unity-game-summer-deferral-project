﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    public float waitToRespawn;
    public PlayerMovement thePlayer;
    public GameObject DeathParticle;

    public int coinCount;
    public int coinCountBonus;
    public Text coinText;

    
    public Text scoreText;
    public Text endLevelScore;

    public Image heart1;
    public Image heart2;
    public Image heart3;

    public Sprite heartFull;
    public Sprite heartHalf;
    public Sprite heartEmpty;

    public int maxHealth;
    public int healthCount;
    private bool respawning;

    public ResetWhenRespawn[] objectsToReset;
    public ResetPatrollingEnemies[] patrolstoreset;
    public bool invincible;

    public Text livesText;
    public int startingLives;
    public int currentLives;

    public GameObject gameOverScreen;

    public AudioSource coindSfx;
    public AudioSource healthPickUpSfx;

    public AudioSource mainGameMusic;
    public AudioSource gameOverMusic;
    public AudioSource levelcompleteMusic;

    private checkpointController theCheckPoints;
    public int score;
    // Use this for initialization

    void Start () {
        thePlayer = FindObjectOfType<PlayerMovement>();

        theCheckPoints = FindObjectOfType<checkpointController>();

        healthCount = maxHealth;

        objectsToReset = FindObjectsOfType<ResetWhenRespawn>();
        patrolstoreset = FindObjectsOfType<ResetPatrollingEnemies>();

        if(PlayerPrefs.HasKey("CoinCount"))
        {
            coinCount = PlayerPrefs.GetInt("CoinCount");
        }
        coinText.text = "Coins: " + coinCount;
        
        if (PlayerPrefs.HasKey("PlayerLives"))
        {
            currentLives = PlayerPrefs.GetInt("PlayerLives");
        }
        livesText.text = " x" + currentLives;
        if (PlayerPrefs.HasKey("Score"))
        {
            score = PlayerPrefs.GetInt("Score");
        }
        else
        {
            currentLives = startingLives;
            score = 0;
        }
        scoreText.text = "Score: " + score;

    }
	
	// Update is called once per frame
	void Update () {
		if(healthCount <= 0 && !respawning)
        {
            Respawn();
            respawning = true;
        }
        if(coinCountBonus >= 100)
        {
            currentLives += 1;
            livesText.text = " x" + currentLives;
            coinCountBonus -= 100;
        }
	}

    public void Respawn()
    {
        currentLives -= 1;
        livesText.text = " x" + currentLives;

        if(currentLives > 0)
        {
            StartCoroutine("RespawnCo");
        }
        else
        {
            thePlayer.gameObject.SetActive(false);
            gameOverScreen.SetActive(true);

            mainGameMusic.Stop();
            gameOverMusic.Play();
        }
        
        
    }

    public IEnumerator RespawnCo()
    {
        
        thePlayer.gameObject.SetActive(false);

        Instantiate(DeathParticle, thePlayer.transform.position, thePlayer.transform.rotation);

        yield return new WaitForSeconds(waitToRespawn);

        healthCount = maxHealth;
        respawning = false;
        UpdateHeartMeter();

        if(theCheckPoints.active)
        {
            score = theCheckPoints.newScore;
            coinCount = theCheckPoints.newCoinCount;
        }
        else
        {
            coinCount = 0;
            score = 0;
        }
        
        coinText.text = "Coins: " + coinCount;
        scoreText.text = "Score: " + score;
        thePlayer.transform.position = thePlayer.respawnPosition;
        thePlayer.gameObject.SetActive(true);

        for(int i = 0; i < objectsToReset.Length; i++)
        {
            objectsToReset[i].gameObject.SetActive(true);
            objectsToReset[i].reset();
        }
        for (int i = 0; i < patrolstoreset.Length; i++)
        {
            patrolstoreset[i].gameObject.SetActive(true);
            patrolstoreset[i].reset();
        }
    }

    public void AddCoins(int coinsToAdd)
    {
        coinCount += coinsToAdd;
        coinCountBonus += coinsToAdd;
        coinText.text = "Coins: " + coinCount;
        coindSfx.Play();
    }
    public void AddScore(int scoreToGive)
    {
        score += scoreToGive;
        scoreText.text = "Score: " + score;
    }
    public void hurtPlayer(int damageToTake)
    {
        if(!invincible)
        {
            healthCount -= damageToTake;
            UpdateHeartMeter();

            thePlayer.Knockback();

            thePlayer.hurtsfx.Play();
        }
    }

    public void GiveHealth(int healthToBeGiven)
    {
        healthCount += healthToBeGiven;
        healthPickUpSfx.Play();
        if (healthCount > maxHealth)
        {
            healthCount = maxHealth;
        }
        UpdateHeartMeter();
        
    }

    public void UpdateHeartMeter()
    {
        switch(healthCount)
        {
            case 6:
                heart1.sprite = heartFull;
                heart2.sprite = heartFull;
                heart3.sprite = heartFull;
                return;
            case 5:
                heart1.sprite = heartFull;
                heart2.sprite = heartFull;
                heart3.sprite = heartHalf;
                return;
            case 4:
                heart1.sprite = heartFull;
                heart2.sprite = heartFull;
                heart3.sprite = heartEmpty;
                return;
            case 3:
                heart1.sprite = heartFull;
                heart2.sprite = heartHalf;
                heart3.sprite = heartEmpty;
                return;
            case 2:
                heart1.sprite = heartFull;
                heart2.sprite = heartEmpty;
                heart3.sprite = heartEmpty;
                return;
            case 1:
                heart1.sprite = heartHalf;
                heart2.sprite = heartEmpty;
                heart3.sprite = heartEmpty;
                return;
            case 0:
                heart1.sprite = heartEmpty;
                heart2.sprite = heartEmpty;
                heart3.sprite = heartEmpty;
                return;         
            default:            
                heart1.sprite = heartEmpty;
                heart2.sprite = heartEmpty;
                heart3.sprite = heartEmpty;
                return;
        }
    }

    public void addLives(int livesToAdd)
    {
        currentLives += livesToAdd;
        livesText.text = " x" + currentLives;
    }
}
