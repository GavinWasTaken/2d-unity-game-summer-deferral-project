﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is used for the camera in the infinite runner gametype.
// the camera in that game type must following the player but be more stiff
public class InfiniteRunnerCameraController : MonoBehaviour {
    // instance of the players tranform
    public Transform thePlayer;

    // the distance the camera will have to be forward at
    public float forwardDistance;

    // the y-axis to set the camera at
    public float yAxis;

    // the z-axis to set the camera at
    public float zAxis;
	// Update is called once per frame
	void Update () {
        // this sets the camera at a single position that will follow the player along 
        // the x-axis plus the foward distance
        transform.position = new Vector3(thePlayer.position.x + forwardDistance, yAxis, zAxis);	
	}
}
//[GAVIN END]