﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//This script is used to icrease the score as the player moves allow the infinite level and the longer
// they last the more score they will accumilate
public class InfiniteRunnerManager : MonoBehaviour {
    // instance of the level manager
    private LevelManager theLevelManager;

    // instance of the infiniterunnerplayercontroller
    private InfiniteRunnerPlayerController theplayer;

    // the score count
    public float scoreCount;

    // the amount of points to give persecond
    public float pointsPerSecond;

    // bool for whether the score is increasing or not
    public bool scoreIncreasing;
	// Use this for initialization
	void Start () {
        // sets the instance of the InfiniteRunnerPlayerController to that of a gameobject with the InfiniteRunnerPlayerController attached
        theplayer = FindObjectOfType<InfiniteRunnerPlayerController>();

        // sets the instance of the level manager to that of a gameobject with the levelmanager attached
        theLevelManager = FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
        // while the player is active within the scene the bool
        // for the scoreincreasing will be set to true
        if(theplayer.gameObject.active)
        {
            scoreIncreasing = true;
        }
        // if not active it will be set to false
        else
        {
            scoreIncreasing = false;
        }
        //if the scoreincreasing is set to true then score count shall increase with the scorecount equalling points per second being
        // increase by the time 
        if (scoreIncreasing)
        {
            scoreCount += pointsPerSecond  * Time.deltaTime;
            // devides the score so it equals something more reasonable
            theLevelManager.AddScore((int)scoreCount / 5);
        }

        
	}
}
//[GAVIN END]