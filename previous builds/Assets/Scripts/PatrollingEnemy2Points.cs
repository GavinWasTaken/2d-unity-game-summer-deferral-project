﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrollingEnemy2Points : MonoBehaviour {
    public Transform leftpoint;
    public Transform rightPoint;

    public float moveSpeed;

    private Rigidbody2D myRigidBody;
    private ResetWhenRespawn check;
    public bool movingRight;
    
	// Use this for initialization
	void Start () {
        myRigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(movingRight && transform.position.x > rightPoint.position.x)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            movingRight = false;
        }
        if (!movingRight && transform.position.x < leftpoint.position.x)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            movingRight = true;
        }

        if(movingRight)
        {
            
            myRigidBody.velocity = new Vector2(moveSpeed, myRigidBody.velocity.y);
        }
        else
        {
            
            myRigidBody.velocity = new Vector2(-moveSpeed, myRigidBody.velocity.y);
        }
    }
}
