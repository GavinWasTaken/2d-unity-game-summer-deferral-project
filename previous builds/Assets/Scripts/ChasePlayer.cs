﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is used primarily in the wave level when the enemies are need to follow the player.
// This script helps the enmies to track the player and avoid small obstacles such as ledges
public class ChasePlayer : MonoBehaviour
{

    // instance of the playermovement script
    public PlayerMovement thePlayer;

    //The rigidbody2d is an object set here in order to use physics with this game object. 
    //This is very useful when you want the object to have a velocity or gravity or other physics attributes
    public Rigidbody2D myrigidbody;

    // the value used for setting how high the enemy can jump over objects
    public float jumpspeed;
    // sets the speeds at which the enemies can move
    public float movespeed;
    // bool used to check wether the layer is on the gameobjects left or right
    public bool isPlayerRight;
    // bool used to check if the player is above the gameobject
    public bool playerIsAbove = false;
    // bool used to set if the gameobject should be jumping or not
    public bool jump = false;
    // float used for the distance between the game object and the player
    public float distanceBetweenPlayerAndPlatform;

	// Use this for initialization
	void Start () {

        // gets the rigidbodys that is attched to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myrigidbody = GetComponent<Rigidbody2D>();

        // find objects within the scene that have the playermovement sccript and assigns an instance of them to the 'thePlayer' instance.
        // since there is only one player within the scene at a time then this instance will be used
        thePlayer = FindObjectOfType<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {

        //The game object uses the position o the theplayer instance's x-axis position to help set the 'isPlayerRight' boolean
        // and to set the gameobjects localscale which will make the gameonject face towards to player
        if (isPlayerRight && thePlayer.transform.position.x < transform.position.x)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            isPlayerRight = false;
        }
        if (!isPlayerRight && thePlayer.transform.position.x > transform.position.x)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            isPlayerRight = true;
        }
        // this will take the position of this gameobjects  and then make it move towards the players position at the move speed set
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(thePlayer.transform.position.x,0), movespeed * Time.deltaTime);

        // this if statement compares the gameobjects y-axis position with the players and if the players is more than the gameobjects then it is set that the player is above the gameobject.
        if (thePlayer.transform.position.y > transform.position.y + 1)
        {
            playerIsAbove = true;
        }
        else
        {
            playerIsAbove = false;
        }
        //if the jump bool is set to  true then the game object y-axis will be effects by the jumpspeed 
        //given making them go upwards while maintaining their x-axis velocity
        if(jump)
        {

            myrigidbody.velocity = new Vector2(myrigidbody.velocity.x, jumpspeed);
            jump = false;
        }
        // if it is false the y velocity remains the same
        else
        {
            
            myrigidbody.velocity = new Vector2(myrigidbody.velocity.x, myrigidbody.velocity.y);
        }

    }

    // the onTriggerStay2d is used for when the colliders of a 2d objects collide and remain collided with one another
    public void OnTriggerStay2D(Collider2D collision)
    {
        // if the colliders gameobject has a tag of 'Ground'
        if (collision.tag == "Ground")
        {
            //sets jump boolean to true
            jump = true;
        }
    }
    // when a gameobjects collided enters into another gameobjects collider
    public void OnTriggerEnter2D(Collider2D collision)
    {
        // if the colliders gameobject has a tag of 'Ground'
        if (collision.tag == "Ground")
        {
            //sets jump boolean to true
            jump = true;
        }
    }


}
// [GAVIN END]