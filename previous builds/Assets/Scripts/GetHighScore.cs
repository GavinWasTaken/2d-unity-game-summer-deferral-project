﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetHighScore : MonoBehaviour {
    public string[] scores;
	// Use this for initialization
	IEnumerator Start () {
        WWW scoreData = new WWW("https://gavinhickeyty4.000webhostapp.com/scoresdata.php");
        yield return scoreData;
        string scoreDataString = scoreData.text;
        print(scoreDataString);
        scores = scoreDataString.Split(';');
        print(GetDataValue(scores[0], "PlayerName:"));
        print(GetDataValue(scores[0], "Score:"));
    }

    public string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|"))
        {
            value = value.Remove(value.IndexOf("|"));
        }
        return value;
    } 
}
