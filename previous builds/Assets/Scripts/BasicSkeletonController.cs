﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicSkeletonController : MonoBehaviour {

    public float moveSpeed;
    public bool canMove;

    private Rigidbody2D myRigidBody;
	// Use this for initialization
	void Start () {
        myRigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(canMove)
        {
            myRigidBody.velocity = new Vector2(-moveSpeed, myRigidBody.velocity.y);
        }
	}
    private void OnBecameVisible()
    {
        canMove = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "KillZone")
        {
            //Destroy(gameObject);
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        canMove = false;
    }
}
