﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this is a script used within another sub game type in which the player must battle aggainst incoming hordes of enemies
// in order to progresss to the next level
public class WaveLevelManager : MonoBehaviour {
    // enums used for the different spawning states within the game
    public enum SpawnState {SPAWNING, WAITING, COUNTING };
    
    // a serialiazable class used to store information of each individual wave 
    // allowing diffferent waves to be customisable
    [System.Serializable]
    public class Wave
    {
        // name of the wave
        public string name;

        // array of the different flying enemies wiithin the wave
        public GameObject[] flyingEnemies;

        // an array of the different ground enemies within the wave
        public GameObject[] groundEnemies;


        // a count for how many enemies to be spawned in
        public int count;

        // the rate at which they will be spawned in
        public float rate;
    }

    // ana rray of the wave class
    public Wave[] waves;

    // the endgameobject that will only spawn once all waves are completed
    public GameObject endGame;

    // the positions for the enemy arrays
    public Transform[] groundEnemySpawnPoint;
    public Transform[] flyingEnemySpawnPoints;

    // nextwave number used to increment throught he waves
    private int nextwave = 0;

    // time between when each wave will begin
    public float timeBetweenWaves = 5f;

    // countdown till the wave when it will begin
    public float waveCountDown;

    //coutndown used to check if all enmies are destroyed
    private float checkEnemiesAliveCountdown = 1f;

    //sets the spawnstate to be counting at the beginning
    private SpawnState state = SpawnState.COUNTING;
	// Use this for initialization
	void Start () {
        // sets the countdown to the time between waves
        waveCountDown = timeBetweenWaves;
	}
	
	// Update is called once per frame
	void Update () {
        // if the current wave spawn state is WAITING then
        // we check whether there are enmies are left if so then the wave is complete and 
        // another function is activated
        if(state == SpawnState.WAITING)
        {
            if(!EnemyIsAlive())
            {
                wavecompleted();
            }
            else
            {
                return;
            }
        }

        // when the wave countdown reaches 0 or less than 0
        // then if the spawn state is set to spawning we begin the spawn co routine
		if(waveCountDown <= 0)
        {

            if(state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextwave] ));
            }
        }
        // wave coutdown counts down
        else
        {
            waveCountDown -= Time.deltaTime;
        }
	}

    // a funciton used to check how many enemies are left by count how many gameobject with the enemeyhealthmanager scripts
    // are within the screen.
    public bool EnemyIsAlive()
    {
        checkEnemiesAliveCountdown -= Time.deltaTime;
        if (checkEnemiesAliveCountdown <= 0f)
        {

            checkEnemiesAliveCountdown = 1f;
            if (GameObject.FindObjectOfType<EnemyHealthManager>() == null)
            {
                return false;
            }
        }
        return true;

    }
    // a coroutine whih switches the spawn state to spawning and then loops through the wave count and 
    // uses a seperate function to spawn in the various enmies at the appropriate rate given.
    IEnumerator SpawnWave(Wave wave)
    {
        Debug.Log("Spawning wave :" + wave.name);
        state = SpawnState.SPAWNING;

        for(int i = 0; i < wave.count; i++)
        {
            SpawnEnemy(wave.groundEnemies, wave.flyingEnemies);
            yield return new WaitForSeconds(1f / wave.rate);
        }
        // state is switch back to WAITING when the loop is finished
        state = SpawnState.WAITING;
        yield break;
    }

    // function used to switch the spawn state back to COUNTING  and if the next wave count is more that the total amount of waves then the 
    // game end object if set to active allowing the player to finish the game otherwise it continues onto the nextwave
    void wavecompleted()
    {

        Debug.Log("Wave completed!");

        state = SpawnState.COUNTING;
        waveCountDown = timeBetweenWaves;

        if(nextwave +1 > waves.Length -1)
        {
            nextwave = 0;
            endGame.SetActive(true);
            Debug.Log("Completed All Waves! Now Looping...");

        }


        else
        {
            nextwave++;
        }
        
    }
    // function used to spawn in the given arrays of enemies at a random spawn points within given arrays of spawn points
    void SpawnEnemy(GameObject[] groundEnemies, GameObject[] flyingEnemies)
    {
        
        Transform _gsp = groundEnemySpawnPoint[Random.Range(0, groundEnemySpawnPoint.Length)];
        Transform _fsp = flyingEnemySpawnPoints[Random.Range(0, flyingEnemySpawnPoints.Length)];


        Instantiate(groundEnemies[Random.Range(0, groundEnemies.Length - 1)], _gsp.position, _gsp.rotation);
        Instantiate(flyingEnemies[Random.Range(0, flyingEnemies.Length - 1)], _fsp.position, _fsp.rotation);
        foreach(GameObject gEnemy in groundEnemies)
        {
            Debug.Log("Spawning Enemy: " + gEnemy.name);
        }
        foreach(GameObject fEnemy in flyingEnemies)
        {
            Debug.Log("Spawning Enemy: " + fEnemy.name);
        } 
    }
}
//[GAVIN END]