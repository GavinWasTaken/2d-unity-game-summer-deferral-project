﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArrow : MonoBehaviour {
    public Rigidbody2D myrigidbody;
    public GameObject impactParticle;
    private Vector3 lastPlayerPos;
    public int damageToGive;
    public float speed;
    public float mindistance;
    public float shotheight;
    private PlayerMovement thePlayer;
    private LevelManager theLevelManager;
    // Use this for initialization
    void Start () {
        myrigidbody = GetComponent<Rigidbody2D>();
        theLevelManager = FindObjectOfType<LevelManager>();
        thePlayer = FindObjectOfType<PlayerMovement>();
        float distbetween = Vector3.Distance(thePlayer.transform.position, gameObject.transform.position);
        Debug.Log(distbetween);
        //lastPlayerPos = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y + shotheight, thePlayer.transform.position.z);
        if(distbetween < mindistance)
        {
            lastPlayerPos = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y + 1, thePlayer.transform.position.z);
        }
        else
        {
            lastPlayerPos = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y + shotheight, thePlayer.transform.position.z);
        }

        if (thePlayer.transform.position.x > gameObject.transform.position.x)
        {
            
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        }
        //else {
        //    myrigidbody.velocity = new Vector2(speed, myrigidbody.velocity.y);
        //}

    }
    private void FixedUpdate()
    {
        this.transform.position = Vector2.MoveTowards(transform.position, lastPlayerPos, speed * Time.deltaTime);
    }
    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            theLevelManager.hurtPlayer(damageToGive);

        }
        Instantiate(impactParticle, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
