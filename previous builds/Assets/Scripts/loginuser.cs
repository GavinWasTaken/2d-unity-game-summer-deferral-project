﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loginuser : MonoBehaviour {
    public string inputUsername;
    public string inputPassword;

    string loginUrl = "https://gavinhickeyty4.000webhostapp.com/verifyUser.php";

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.L))
        {
            StartCoroutine (LoginToDb(inputUsername, inputPassword));
        }
	}

    IEnumerator LoginToDb(string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);

        WWW www = new WWW(loginUrl, form);
        yield return www;
        Debug.Log(www.text);
    }
}
