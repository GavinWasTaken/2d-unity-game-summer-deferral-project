﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastRadius : MonoBehaviour {
   
    public int damageToGive;
    private LevelManager theLevelManager;
    // Use this for initialization
    void Start () {
        theLevelManager = FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            theLevelManager.hurtPlayer(damageToGive);
        }
        else if (collision.tag == "Enemy")
        {
            collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
        }
    }
}
