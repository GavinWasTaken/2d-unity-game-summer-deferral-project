﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is gievn the life pickups iwithin the game to help the player recover health
public class HealthPickUp : MonoBehaviour {
    //integer for the health to be given
    public int healthtobegiven;
    
    //instance of the levelmanager
    private LevelManager theLevelManager;
	// Use this for initialization
	void Start () {
        // sets the instanc eof the level manager to that of a gameobject with the levelmanager attached
        theLevelManager = FindObjectOfType<LevelManager>();
	}

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {

        // if the gameobjects collider comes into contactt with another collider whos tag is 'Player'
        if (collision.tag == "Player")
        {
            // adds the health to the players health suing the GiveHealth method in the level manager
            theLevelManager.GiveHealth(healthtobegiven);
            // sets the game object to inactive then
            gameObject.SetActive(false);
        }
    }
}
//[GAVIN END]