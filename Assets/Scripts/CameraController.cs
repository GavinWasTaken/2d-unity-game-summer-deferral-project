﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script helps control the camera which will follow the player smoothly and stop and set its self when needed
public class CameraController : MonoBehaviour {

    // instance of the gameobject that is the target for the camera to folow
    public GameObject target;

    //set the float for which the camera will use for the smooth trasition during the players respawn
    public float respawnSmoothing;
    //set the float for which the camera will use for the smooth trasition during the players reset
    public float resetSmoothing;

    //the float used to set the camera forward of the player depends of the number given
    public float followAhead;

    // set the smoothing number used by the LERP of the camera
    public float smoothing;

    // bool to tell if the camera if following the player
    public bool followingtarget;

    // the height restriction given to the camera to make sure it stays within a certain amount of bounds
    public float cameraLevelHeight;

    // another height restriction given to the camera
    public float heightRestrict;

    // instance of the playermovement script
    private PlayerMovement thePlayer;

    // sets up the vector used to find the targets position that the camera is to follow
    private Vector3 targetPosition;

	// Use this for initialization
	void Start () {
        // at the start the camera is set to follow the player
        followingtarget = true;

        // used to find the player by finding an object with the playersmovement script
        thePlayer = FindObjectOfType<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {

        //if the player is not visible within camera
        if(!thePlayer.GetComponent<Renderer>().isVisible)
        {
            //the smoothing is set to the respawn smoothing
            smoothing = respawnSmoothing;
        }

        //if the player is visible within camera
        else if (thePlayer.GetComponent<Renderer>().isVisible)
        {
            //the smoothing is set to the reset smoothing
            smoothing = resetSmoothing;
        }

        //if the camera is following its target 
        if (followingtarget)
        {
            // when the players position on the y axis is less than the heightrestriction then the camera will not go further down the y-axis
            if(thePlayer.transform.position.y <= heightRestrict)
            {
                targetPosition = new Vector3(target.transform.position.x, transform.position.y , transform.position.z);
            }

            // else camera will following the player 
            else
            {
                targetPosition = new Vector3(target.transform.position.x, target.transform.position.y +cameraLevelHeight, transform.position.z);
            }

            // if the targets localscale changes on the x-axis ie the player changes their direction the 
            // camera will change accordingly and change its positioning to mathc its previous position but flipped
            if (target.transform.localScale.x > 0f)
            {
                targetPosition = new Vector3(targetPosition.x + followAhead, targetPosition.y, targetPosition.z);
            }
            else
            {
                targetPosition = new Vector3(targetPosition.x - followAhead, targetPosition.y, targetPosition.z);
            }

            //interpolates between two vectors, wwith the smoothing this is made so the there is a smooth trasition of the camera and it does not snap too fast.
            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing * Time.deltaTime);
        }
    }
}
// [GAVIN END]