﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Script to be using with varios projectiles so that they will not continue 
//to exist within the game if they do not interact with anything

public class DestroyOverTime : MonoBehaviour {

    // the float of the lifetime of the object
    public float lifeTime;

	
	// Update is called once per frame
	void Update () {
        // the lifetime counts down
        lifeTime = lifeTime - Time.deltaTime;
        // if the lifetime reaches zero or less than zero then the object is destroyed
        if(lifeTime <= 0f)
        {
            Destroy(gameObject);
        }
	}
}
//[GAVIN END]