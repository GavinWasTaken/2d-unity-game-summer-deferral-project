﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//this script is used for checkpoints within the game where when activated will 
//reset the point at which the player will respawn to upon their death.
// it will also reset any of the appropriate collectibles once the checkpoint is reached
public class checkpointController : MonoBehaviour {

    //sets the sprite used to associate with the checkpoint being active
    public Sprite active;

    //sets the sprite used to associate with the checkpoint being active
    public Sprite inactive;

    // bool used to set when the checkpoint is active
    public bool checkpointActive;

    // an array used to hold the dropcollectible objects within the game
    public GameObject[] dropCollectibles;

    // an array used to hold the collectible objects within the game
    public GameObject[] collectibleBehind;

    //sets an instance of the levelmanager script
    private LevelManager theLevelManager;

    // set an instaance of the gameobbjects sprite renderer which is used to display images for game objects
    private SpriteRenderer thisSpriteRenderer;
    
    // integer for a new coin count 
    public int newCoinCount;
    
    // integer for a new score
    public int newScore;

    // Use this for initialization
    void Start () {
        //sets the array with all the objects within the scene that have the tag 'Collectibles'
        collectibleBehind = GameObject.FindGameObjectsWithTag("Collectible");
        
        // sets this instance with the spriterenderer component attached to this gameobject
        thisSpriteRenderer = GetComponent<SpriteRenderer>();

        //sets this instance to the level manager within the scene
        theLevelManager = FindObjectOfType<LevelManager>();
        
    }
	
	// Update is called once per frame
	void Update () {

        //sets the array with all the objects within the scene that have the tag 'DropCollectibles'
        dropCollectibles = GameObject.FindGameObjectsWithTag("DropCollectible");

        


    }
    // when a gameobjects collided enters into another gameobjects collider
    public void OnTriggerEnter2D(Collider2D collision)
    {
        // if the colliding gamobject has a tag of 'Player'
        if(collision.tag == "Player")
        {

            // the gameobjects sprite changes to the active sprite
            thisSpriteRenderer.sprite = active;

            //its bool is set to active
            checkpointActive = true;
            
            // when the bool is set to active
            if (checkpointActive)
            {

                //gets the coin count from the level manager and sets the new coin count to be equal to this coin count
                newCoinCount = theLevelManager.coinCount;
                //gets the score from the level manager and sets the newscore to be equal to this score
                newScore = theLevelManager.score;

                // this will lopp through each gameobject within the 'collectiblebehind' array carrying out the below functions on each 
                foreach (GameObject collectible in collectibleBehind)
                {
                    // this will check to see if any of the gameobjects have an x-axis position less than the checkpoints position
                    // and if it does the object will be set to inactive
                    if (collectible.transform.position.x <= transform.position.x)
                    {
                        collectible.SetActive(false);
                    }
                    // if the object is ahead of the checkpoint if will be set to active
                    else if (collectible.transform.position.x > transform.position.x)
                    {
                        collectible.SetActive(true);
                    }
                    // else it will just return
                    else
                    {
                        return;
                    }
                }
            }
            // this loop will go through the the collectibles dropped by enemies and set them to inactive 
            foreach (GameObject dropcollectible in dropCollectibles)
            {
                dropcollectible.SetActive(false);
            }
        }
}

}
//[GAVIN END]