﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script is used to give a blastradius of the bomb gameobject used within the game.
// the blastradius will inflict damge to both players and enemies within its collision
public class BlastRadius : MonoBehaviour {

   // an integer for the damage the blast can cause
    public int damageToGive;

    // private instance of the Level Manager script
    private LevelManager theLevelManager;

    // Use this for initialization
    // start is a key function within the Unity engine. Code within this function will only be used once upon 
    // that start of a scene or when the game object that this code i sattached to is initialised
    void Start () {

        // The code finds an object within the games hierarchy that has a level manager script attached. Since there is only one level manager object per game scene we onl need to search for one object. This is then assigned to the variable 'theLevelManager'
        theLevelManager = FindObjectOfType<LevelManager>();
	}

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the object it collides with has a tag of 'Player'
        if(collision.tag == "Player")
        {
            // inflicts damage on the player using the levelmangers hurtplayer to 
            // inflict damage equal to the 'damageToGive' integer given from this script
            theLevelManager.hurtPlayer(damageToGive);
        }
        // else if the object it collides with has a tag of 'Enemy'
        else if (collision.tag == "Enemy")
        {
            // get a component with the enemyhealthmanager script form the colliding object and 
            // then use its 'ApplyDamage' function to inflict damage to the enemy
            collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
        }
    }
}
// [GAVIN END]
