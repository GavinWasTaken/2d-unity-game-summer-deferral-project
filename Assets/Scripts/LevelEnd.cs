﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
// script used at the end off each level where there is another level to continue onto
public class LevelEnd : MonoBehaviour {

    //variable used to load the next level
    //set the score
    //instance of the player, the camera and the level manager
    // the end level score
    //the waittime till the next level
    // and the pause given at the end of a level before the playercharacter move off screen
    public string levelToLoad;
    public static int score;
    private PlayerMovement thePlayer;
    private CameraController theCamera;
    private LevelManager theLevelManager;
    public GameObject endScore;
    public Text endScoreText;
    public float endScoreWaitTime;
    public float waitTillMove;
    public float waitToLoadLevel;

    private bool movePlayer;
	// Use this for initialization
	void Start () {
        // setting the instn=ance by finding them within the scene
        thePlayer = FindObjectOfType<PlayerMovement>();
        theCamera = FindObjectOfType<CameraController>();
        theLevelManager = FindObjectOfType<LevelManager>();
        //theLevelManager = GetComponent<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
        // if the player can move then the player will move with a velocity given and not from player input
		if(movePlayer)
        {
            thePlayer.myRigidbody.velocity = new Vector2(thePlayer.movespeed, thePlayer.myRigidbody.velocity.y);
        }
	}
    // OntriggerEnter2D is used with colldders within the game enter one another colliders


    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'PLayer'
        if (collision.tag == "Player")
        {
            // starts the coroutine 'LevelEndCo'
            StartCoroutine("LevelEndCo");
            //SceneManager.LoadScene(levelToLoad);
        }
    }

    // this function starts by temporarily freezing the player to the spot and making them invicible
    // then by using the level manager instance it gets all the information regarding, coins collected, score earned, amount of the players health and
    // amount of player lives
    // it changes the games music from the main theme to the completed level theme
    //it resets the players velocity to zwro making them stop 
    //after and certain amount of seconds thye makes the player able to move and displays a scene momentarilty to show the players current score
    // then it loads the next level given
    public IEnumerator LevelEndCo()
    {
        
        thePlayer.canmove = false;
        theCamera.followingtarget = false;
        theLevelManager.invincible = true;
        PlayerPrefs.SetInt("CoinCount", theLevelManager.coinCount);

        PlayerPrefs.SetInt("PlayerLives", theLevelManager.currentLives);

        PlayerPrefs.SetInt("Score", theLevelManager.score);

        PlayerPrefs.SetInt("ScoreText", theLevelManager.score);

        theLevelManager.mainGameMusic.Stop();
        theLevelManager.levelcompleteMusic.Play();
        thePlayer.myRigidbody.velocity = Vector3.zero;
        
        yield return new WaitForSeconds(waitTillMove);
        movePlayer = true;
        yield return new WaitForSeconds(waitToLoadLevel);
        thePlayer.gameObject.SetActive(false);
        endScoreText.text = "Final Score: " + theLevelManager.score;
        endScore.SetActive(true);
        yield return new WaitForSeconds(endScoreWaitTime);
        endScore.SetActive(false);
        thePlayer.gameObject.SetActive(true);
        SceneManager.LoadScene(levelToLoad);

    }
}
//[GAVIN END]
