﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// This script is used to get the top ten high scores for the 
// two different gametypes using an online database and php scripts
public class GetHighScore : MonoBehaviour {
    
    // the text that will be used to display the top ten scores
    public Text scorelist;
    // the url for the php script to get the top ten scores for the gametype 'Infinite Runner' in descending order
    public string infiniterunnerurl = "https://gavinhickeyty4.000webhostapp.com/TopTenPlayers.php";

    // the url for the php script to get the top ten scores for the gametype 'MainGame' in descending order
    public string maingamersurl = "https://gavinhickeyty4.000webhostapp.com/TopTenMainGamers.php";
    // Use this for initialization

    // calls the top ten infinite runners
    public void callTopTenRunners(string url)
    {
        // resets the score text to empty for the next top ten scores
        scorelist.text = string.Empty;
        url = infiniterunnerurl;
        StartCoroutine("showScore", url);
    }
    // calls the top ten main game player scores
    public void calltoptenmaingamers(string url)
    {
        // resets the score text to empty for the next top ten scores
        scorelist.text = string.Empty;
        url = maingamersurl;
        
        StartCoroutine("showScore", url);
    }

    // a Coroutine used to take a url and gather a respond from and one of the online data scripts 
    IEnumerator showScore(string url)
    {
        WWW scoreData = new WWW(url);
        yield return scoreData;
        string scoreDataString = scoreData.text;
        scorelist.text = scoreDataString;
        print(scoreDataString);
    }
}

//[GAVIN END]