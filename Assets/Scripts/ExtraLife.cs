﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//script which is used with objects that add lives to the player
public class ExtraLife : MonoBehaviour {
    // the amount of lives to give
    public int livesToGive;

    // instance of the level manager
    private LevelManager theLevelManager;
	// Use this for initialization
	void Start () {
        //sets the level manager instance to the level manager found within the scene
        theLevelManager = FindObjectOfType<LevelManager>();
    }

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contactt with another collider whos tag is 'Player'

        if (collision.tag == "Player")
        {
            //uses the function from the level manager to add lives increase the players lives by the amount given
            theLevelManager.addLives(livesToGive);

            // sets the gameobject to inactive in the scene
            gameObject.SetActive(false);
        }
        
    }
}
//[GAVIN END]