﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// this script is used to submit a new player to the database with the use of entered variables by the player
public class InsertUser : MonoBehaviour {

    //string that will hold the inputted data from the player
    public string inputname;
    public string inputemail;
    public string inputPassword;

    // input fields used by the user to input data into the mentioned string
    public InputField createUsername;
    public InputField createEmail;
    public InputField createPassword;
    //if a user is create this bool be set to true 
    public bool userCreated;

    // messages thee player will recieve based on the outcome
    public GameObject createFailedMessage;
    public GameObject createSuccessMessage;

    // the duration of the messages on the screen
    public float messageTime;

    // the online php script
    string createuserUrl = "https://gavinhickeyty4.000webhostapp.com/InsertUser.php";
    // Use this for initialization

    // this functtion will be used to gather the information from the user into string and then use these string with the coroutine 
    // to submit a new account to the online database
    public void createUser()
    {
        inputname = createUsername.text;
        inputemail = createEmail.text;
        inputPassword = createPassword.text;
        StartCoroutine(CreateUser(inputname, inputemail, inputPassword));
    }

    // this coroutine will use a www form to o help submit the data enetered by the player 
    // to the ww class for it to submit to the online script 
    IEnumerator CreateUser(string username, string email, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("emailPost", email);
        form.AddField("passwordPost", password);

        // www class uses the online scripts url to submit the data to 
        WWW www = new WWW(createuserUrl, form);
        yield return www;
        Debug.Log(www.text);
        // this checks the respond for a specific replay and if that reply is given then 
        // the success message is shown to the player
        if(www.text.Contains("everything ok"))
        {
            userCreated = true;
            createSuccessMessage.SetActive(true);
            yield return new WaitForSeconds(messageTime);
            createSuccessMessage.SetActive(false);
            PlayerPrefs.SetString("UserName", username);
        }
        // otherwise the player is shown the fail message
        else
        {
            userCreated = false;
            createFailedMessage.SetActive(true);
            yield return new WaitForSeconds(messageTime);
            createFailedMessage.SetActive(false);
        }
    }
}
//[GAVIN END]