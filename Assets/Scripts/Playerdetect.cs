﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is used to detect the player when near the enemy and activate the enemys attack animation
public class Playerdetect : MonoBehaviour {
    // instance of the animator
    private Animator animation;
    // and instance of the rigidbody2d
    public Rigidbody2D myrb;
	// Use this for initialization
	void Start () {
        // gets the animator that is attached to the gameobject and sets that animator to animator for futher used in the code
        animation = GetComponent<Animator>();

        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myrb = GetComponent<Rigidbody2D>();
    }

    // OntriggerEnter2D is used with collders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'PLayer'
        if (collision.tag == "Player")
        {
            //sets the velocity of the enemy to 0 to make it stop
            myrb.velocity = new Vector2(0,0);
            //activates the attack animation using its parameter
            animation.SetBool("attack", true);
        }
    }
    // OntriggerExit2D is used with collders within the game exit one another colliders
    private void OnTriggerExit2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contact with another collider whos tag is 'PLayer'
        if (collision.tag == "Player")
        {
            // deactivates the attack animation using the animations parameters
            animation.SetBool("attack", false);
        }
    }
}
//[GAVIN END]