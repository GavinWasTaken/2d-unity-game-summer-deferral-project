﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this script is used on the mages dark orbs which are fired at the players last position
public class EnemyOrb : MonoBehaviour {
    
    // instance of the rigidbody2d
    public Rigidbody2D myrigidbody;

    // the impact particle
    public GameObject impactParticle;

    // integer for the damage to be given
    public int damageToGive;

    // float for the speed of the orb
    public float speed;

    // instance of the player
    private PlayerMovement thePlayer;

    // instance of the level manager
    private LevelManager theLevelManager;
    // Use this for initialization
    void Start () {

        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myrigidbody = GetComponent<Rigidbody2D>();

        //sets the level manager instance to the level manager found within the scene
        theLevelManager = FindObjectOfType<LevelManager>();

        //sets the the player instance to the player found within the scene
        thePlayer = FindObjectOfType<PlayerMovement>();
    }

    private void FixedUpdate()
    {
        // moves the gameobject towards the players position at the speed given
        this.transform.position = Vector2.MoveTowards(transform.position, new Vector2(thePlayer.transform.position.x, 0), speed * Time.deltaTime);
    }

    // OntriggerEnter2D is used with colldders within the game enter one another colliders

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        if (collision.collider.tag == "Player")
        {
            // uses the levelmanagers hurtplayer function with the damageToGive integer
            theLevelManager.hurtPlayer(damageToGive);

        }
        // spawns the impact particle at the gameobjects position and rotation
        Instantiate(impactParticle, transform.position, transform.rotation);
        // destroys the gameobject removing it from  the scene
        Destroy(gameObject);
    }
}
//[GAVIN END]