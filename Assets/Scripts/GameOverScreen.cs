﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
// This script is used witn the game over scene to help set different 
// methods o the right buttons within the inspector of the unity engine
public class GameOverScreen : MonoBehaviour {
    // a string to be used to help load the main menu scene
    public string mainMenu;
    
    // Ui text to display the score
    public Text endScore;

    // string used to set the gametype being curretly played
    public string gameType;

    // instance of the level manager script
    private LevelManager theLevelManager;
	// Use this for initialization
	void Start () {
        //sets the level manager instance to the level manager found within the scene
        theLevelManager = FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
        //continually updates the score text to make the current score the player has
        endScore.text = "Your Score: " + theLevelManager.score;
	}
    // function for restarting the game
    public void Restart()
    {
        //Player Prefs stores and access player preferences between game sessions
        // These are useful for setting up data acroos the entire game

        // this sets the amount of coins collect to to the level managers coin count
        PlayerPrefs.SetInt("CoinCount", theLevelManager.coinCount);
        
        // this sets the players live count to the level managers live count
        PlayerPrefs.SetInt("PlayerLives", theLevelManager.startingLives);

        // this sets the score to the level managers score
        PlayerPrefs.SetInt("Score", theLevelManager.score);

        // reloads the current scene being played

        // scenemanager is used to help with loading new scenes in the unity engine
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        // if the scene is the first level then upon restart the the player prefs for the scores
        // will be reset to zero
        if(SceneManager.GetActiveScene().name == "Level 1" )
        {
            PlayerPrefs.SetInt("PlayerLives", theLevelManager.startingLives);
            PlayerPrefs.SetInt("CoinCount", 0);
            PlayerPrefs.SetInt("Score", 0);
        }
    }
    // This will send the the player to the main menu scene
    public void QuitToMainMenu()
    {
        SceneManager.LoadScene(mainMenu);
    }
}
//[GAVIN END]