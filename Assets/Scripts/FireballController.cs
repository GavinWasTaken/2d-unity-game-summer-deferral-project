﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//  script to be used with the fireball to help controll its direction
public class FireballController : MonoBehaviour {
    //speed of the fireball
    public float speed;

    // the gameobjects rigidbody2d
    public Rigidbody2D myrigidbody;
    
    // the impact particle system
    public GameObject impactParticle;

    // integer for the damage to give
    public int damageToGive;

    //instance of the player
    private PlayerMovement thePlayer;
    // Use this for initialization
    void Start () {

        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        myrigidbody = GetComponent<Rigidbody2D>();

        // sets this instance to the object within the scene with the playermovement script
        thePlayer = FindObjectOfType<PlayerMovement>();
        
        // sets the direction the fireball some be facing bacing on on the posiition of the pplayer along the a-xis
        if(thePlayer.transform.localScale.x < 0)
        {
            speed = -speed;
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        }
        // while the W key is pressed while this object is first used the objects 
        // direction will change to make the fireball fire upwards and face upwards
        if (Input.GetKey(KeyCode.W))
        {
            myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, speed, transform.rotation.z * 90);
            //rotates the object to face upwards
            gameObject.transform.Rotate(0, 0, 90);
            if(thePlayer.transform.localScale.x < 0)
            {
                myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, -speed, transform.rotation.z * 90);
                //rotates the object to face upwards
                gameObject.transform.Rotate(0, 0, 180);
            }
        }
        // while the S key is pressed while this object is first used the objects 
        // direction will change to make the fireball fire downwards and face downwards
        else if (Input.GetKey(KeyCode.S))
        {
            myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, -speed, transform.rotation.z * 90);
            gameObject.transform.Rotate(0, 0, -90);
            //rotates the object to face downwards
            if (thePlayer.transform.localScale.x < 0)
            {
                myrigidbody.velocity = new Vector3(myrigidbody.velocity.x, speed, transform.rotation.z * 90);
                //rotates the object to face downwards
                gameObject.transform.Rotate(0, 0, -180);
            }
        }
        // if no extra button is being pressed then thee object doesn't change its rotation or direction
        else
        {
            myrigidbody.velocity = new Vector2(speed, myrigidbody.velocity.y);
        }
    }

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contactt with another collider whos tag is 'Enemy'
        if (collision.tag == "Enemy")
        {
            // used the enemyhealthmanagers funciton to imflict damge on an enemys health
            collision.GetComponent<EnemyHealthManager>().ApplyDamage(damageToGive);
        }
        // if the gameobjects collider comes into contactt with another collider whos tag is 'Boss'
        if (collision.tag == "Boss")
        {
            //uses the function within the bosses script to inflict damge on it
            collision.GetComponent<BossHandHealthManager>().ApplyDamage(damageToGive);
        }
        // spawsn the impact pacticle effect
        Instantiate(impactParticle, transform.position, transform.rotation);
        //destroys the gameobject removing it from the screen
        Destroy(gameObject);
    }
}

//[GAVIN END]