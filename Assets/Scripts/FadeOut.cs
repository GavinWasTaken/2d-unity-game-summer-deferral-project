﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]

using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
// fades the screen out to black as a transition for the end of the level
public class FadeOut : MonoBehaviour {

    //the float forthe time of the fade out
    public float time;

    // uses the ui image 
    private Image blackscreen;
	// Use this for initialization
	void Start () {
        // gets the image from the image component on the gameobject 
        blackscreen = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        //changes the images alpha from its current alpha to another over the time given
        blackscreen.CrossFadeAlpha(0f, time, false);
        
        //if the image aplha components color is 0
        if(blackscreen.color.a == 0)
        {
            // set the gameobject to inactive
            gameObject.SetActive(false);
        }
	}
}

//[GAVIN END]