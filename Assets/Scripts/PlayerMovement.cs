﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
// This script is in charge ont the layers controls and varvious actions the the player can do within the game.
// as well as the various interactions with other colliders
public class PlayerMovement : NetworkBehaviour {
    // the players movespeed
    public float movespeed;

    // a temporary movespeed set initialised o 0
    private float tempmovespeed = 0;

    // active move speed
    private float activemovespeed;

    // check used to see if the player can move
    public bool canmove;

    //The rigidbody2d is an object set here in order to use physics with this game object. 
    //This is very useful when you want the object to have a velocity or gravity or other physics attributes
    public Rigidbody2D myRigidbody;

    // the jump speed used to swet the jump height
    public float jumpspeed;

    // transform used to set the position of the players firepoint
    public Transform firepoint;

    // gameobject of the fireball instance
    public GameObject fireball;

    // a timer for then the player can melee again
    public float meleeTimer;

    //float value forr time to melee
    public float timeToMelee;

    //boolean to check if the player can melee
    public bool readyToMelee = false;

    //float set to be used as a timer to shoot
    public float shootTimer;
    // a time to shoot float
    public float timeToShoot;

    // a boolean for checking if the player can shoot initialised to false
    public bool readyToShoot = false;

    // groundcheck to be used to check if the player is on the ground and not in the air
    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;

    public bool isGrounded;

    // the vertical speed to be used with the animator
    public float vspeed;

    // this gameobject animator
    private Animator myAnim;

    // the position at which the player will be reset to upon death
    public Vector2 respawnPosition;

    //instance of the level manager
    public LevelManager theLevelManager;

    // gameobject used to jump on and damge enemies
    public GameObject stompBox;

    // variable used to implement knockback on the player when they are damageed
    public float knockbackForce;
    public float knockbackLength;
    private float knockbackCounter;

    // small length of time in which the player is immune to damage for exxample when being knocked back
    public float invincibilityLength;
    private float invincibilityCounter;

    // various sound effect to be used with different actions
    public AudioSource jumpsfx;
    public AudioSource hurtsfx;
    public AudioSource swordsound;

    // like the ground check but used with moving platforms within the game
    private bool onPlatform;
    public float onPLatformSpeedModifier;

    // variables used for t=when the player is climbing a ladder
    public bool onLadder;
    public float climbSpeed;
    public float climbVelocity;
    public float gravityStore;

    
    // Use this for initialization
    void Start () {
        //sets the rigidbody2d to the the rigidbody2d component on the gameobject
        myRigidbody = GetComponent<Rigidbody2D>();

        // set myAnim to the animator component of the gameobject
        myAnim = GetComponent<Animator>();

        // set gravitystores to the gravityscale of the rigidbody2d
        gravityStore = myRigidbody.gravityScale;

        //sets the respawnPosition to the be the players position
        respawnPosition = transform.position;

        // sets the instance of the level manager to that of a gameobject with the levelmanager attached
        theLevelManager = FindObjectOfType<LevelManager>();
        
        //sets the  moving speeds and makes the boolean for be ables to move true
        activemovespeed = movespeed;
        tempmovespeed = movespeed;
        canmove = true;
	}
	
	// Update is called once per frame
	void Update () {
        //if(!isLocalPlayer)
        //{
        //    return;
        //}

        // checks within a circle area of the size of the radius =given checking only for the whatIsGround
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        // check to see if the player can move and if the knockbackcounter is less than or equal to zero
        if (knockbackCounter <= 0 && canmove)
        {
            // if the player is on the ladder then they're gravity is set to zero allowing them to go upwards
            // then using the vertical input it changes the velocity allowing the player to go up and down
            if(onLadder)
            {
                myRigidbody.gravityScale = 0f;
                climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");

                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, climbVelocity);
            }
            // when they are not on the ladder their gravity scale is then reset to normal
            // using the gravitystore which hold the original gravity
            if(!onLadder)
            {
                myRigidbody.gravityScale = gravityStore;
            }

            // changes the movespeed while on a platform using multipliers already set
            if(onPlatform)
            {
                activemovespeed = movespeed * onPLatformSpeedModifier;
            }
            else
            {
                activemovespeed = movespeed;
            }
            // gets the vertical speed and sets it using the rigidbody2ds velocity along the y axis
            vspeed = myRigidbody.velocity.y;

            // uses horizontal input which is set in unity as the A and D keys to make the play change there local scale 
            // which changes their direction and implements velocity using  the active move speed
            if (Input.GetAxisRaw("Horizontal") > 0f)
            {
                myRigidbody.velocity = new Vector2(activemovespeed, myRigidbody.velocity.y);
                transform.localScale = new Vector2(1f, 1f);
            }
            else if (Input.GetAxisRaw("Horizontal") < 0f)
            {
                myRigidbody.velocity = new Vector2(-activemovespeed, myRigidbody.velocity.y);
                transform.localScale = new Vector2(-1f, 1f);
            }
            else
            {
                // if no input is being made then the x-axis velocity is set to zero making them stay in the one spot
                myRigidbody.velocity = new Vector2(0f, myRigidbody.velocity.y);
            }

            // If the user presses the space bar which in unity is designated to the jump input
            // then the velocity along the is increased by the jumpspeed
            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpspeed);
                // soundeffect is played as well
                jumpsfx.Play();
                
            }
            //these timers here are used to stops the player from being able to spam the input button to attack repeatedly 

            //Using the melee timer to check when the player may use their melee attack
            if (meleeTimer >= timeToMelee)
            {
                //resets there move speed temporarily
                movespeed = tempmovespeed;
                readyToMelee = true;
                
            }

            else
            {
                readyToMelee = false;
                // timer counts down
                meleeTimer += Time.deltaTime;
            }

            // same is used here except for the fireball attack
            if (shootTimer >= timeToShoot)
            {
                readyToShoot = true;
            }
            else
            {
                readyToShoot = false;
                shootTimer += Time.deltaTime;
            }


            // these function get parameters used in the animator to set up animations
            if(myAnim.GetBool("SpellCast"))
            {
                myAnim.SetBool("SpellCast", false);
            }

            if(myAnim.GetBool("HeavyAttack"))
            {
                myAnim.SetBool("HeavyAttack", false);
                
            }
            // input used for melee attacl
            if (Input.GetKeyDown(KeyCode.K))
            {
                // player can only use the melee attackk while on the ground
                if (readyToMelee && isGrounded)
                {
                    myAnim.SetBool("HeavyAttack", true);
                    movespeed -= movespeed;
                    //attack sound effect played
                    swordsound.Play();
                    meleeTimer = 0;
                }
                
            }

            // input key used to fire a fireball attack
            if (Input.GetKeyDown(KeyCode.F) )
            {
                
                if (readyToShoot)
                {
                    myAnim.SetBool("SpellCast", true);
                    Instantiate(fireball, firepoint.position, firepoint.rotation);
                  shootTimer = 0;
                }
            }
            // sets the invincible to false while there is no knockback
            theLevelManager.invincible = false;
        }
        // if there is knockback on the player
        if(knockbackCounter > 0)
        {
            // knockback timer counts down
            knockbackCounter -= Time.deltaTime;
            // sets the direction of the player so they appear to be knocked backwards and do not change their direction
            if(transform.localScale.x >0)
            {
                myRigidbody.velocity = new Vector2(-knockbackForce, knockbackForce);
            }
            else
            {
                myRigidbody.velocity = new Vector2(knockbackForce, knockbackForce);
            }
        }

        // get the counter for the invincibility
        if(invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;
        }
        if(invincibilityCounter <=0)
        {
            theLevelManager.invincible = false;
        }

        // animator uses rigidbody velocitys to haelp with parameters used to switch between different animations
        myAnim.SetFloat("VerticalSpeed", myRigidbody.velocity.y);
        myAnim.SetFloat("Speed", Mathf.Abs(myRigidbody.velocity.x));
        myAnim.SetBool("Grounded", isGrounded);

        // only activates the stompbox used by the player to inflict damge on enemies when the 
        // players y velocity is less than 0 there implying than they are falling
        if(myRigidbody.velocity.y < 0)
        {
            stompBox.SetActive(true);
        }
        else
        {
            stompBox.SetActive(false);
        }
	}

    // test code used when beginning multiplayer tests not completed
    // used to imply colour to your player character to distiguish between other players
   //public override void OnStartLocalPlayer()
   //{
   //    GetComponent<MeshRenderer>().material.color = Color.blue;
   //}

    // Knock back function used to activate the knockback bool, set the invicibility counter and knockback counter 
    public void Knockback()
    {
        knockbackCounter = knockbackLength;
        invincibilityCounter = invincibilityLength;
        theLevelManager.invincible = true;
    }

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    public void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contactt with another collider whos tag is 'KillZone'
        if (collision.tag == "KillZone")
        {
            //the level managers respawn function is then started
            theLevelManager.Respawn();
        }
        // if the gameobjects collider comes into contactt with another collider whos tag is 'Checkpoint'

        if (collision.tag == "Checkpoint")
        {
            // the players respawn position is change to that of the checkpoint it has reached
            respawnPosition = collision.transform.position;
        }

    }
    // OnCollisionEnter2D is used with layers within the game when they come into contact with other gameobjects with different or same layers
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // if the gameobject they are coming into contact with has a tag of 'MovingPlatform'
        if(collision.gameObject.tag == "MovingPlatform")
        {
            // makes the player a child object of that gameobject and sets it to being on a platform
            transform.parent = collision.transform;
            onPlatform = true;
        }
    }
    // OnCollisionExit2D is used with layers within the game when they leave contact with other gameobjects with different or same layers
    private void OnCollisionExit2D(Collision2D collision)
    {
        // if the gameobject they are coming into contact with has a tag of 'MovingPlatform'
        if (collision.gameObject.tag == "MovingPlatform")
        {
            // the player is then no longer a child object of the platform and is set to
            // not being on the platform anymore
            transform.parent = null;
            onPlatform = false;
        }
    }
}
//[GAVIN END]