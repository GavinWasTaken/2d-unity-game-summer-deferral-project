﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// script used to imply a bounce effect when the play jumps ontop of a stompable enemy
public class StompEnemy : MonoBehaviour {

    // sets the rigidbodys2d as well as the force of the counce given to the player
    private Rigidbody2D playerrigidbody;
    public float bounceForce;
	// Use this for initialization
	void Start () {
        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        playerrigidbody = transform.parent.GetComponent<Rigidbody2D>();
	}

    // OntriggerEnter2D is used with colldders within the game enter one another colliders
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if the gameobjects collider comes into contactt with another collider whos tag is 'Stompable'
        if (collision.tag == "Stompable")
        {
            //implys the bounce force to the player y velocity of the player rigidbody2d
            playerrigidbody.velocity = new Vector2(playerrigidbody.velocity.x, bounceForce);

        }
    }
}
//[GAVIN END]