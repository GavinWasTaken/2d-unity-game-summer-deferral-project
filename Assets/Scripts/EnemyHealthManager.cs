﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Script to be used to manager the health of all the enemies 
public class EnemyHealthManager : MonoBehaviour {
    
    // sets an integer for the max health
    public int maxHealth;

    // set an integer for the enemys health
    public int enemyHealth;

    //a gameobject for the enemys death particle
    public GameObject enemyDeathParticle;

    // array for the object for the enemy to drop
    public GameObject[] dropItems;

    // rigib=dbody2d for the enemy
    public Rigidbody2D enemyRigidbody;

    // set a knockback force for the enemy
    public float knockbackforceY;

    // audio to be used when enemy is hit
    public AudioSource hit;

	// Use this for initialization
	void Start () {
        // gets the rigidbodys that is attached to the gameobject and sets that rigidbody to myrigidbody for futher used in the code
        enemyRigidbody = GetComponent<Rigidbody2D>();

        // enemys health is set to the max health
        enemyHealth = maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
        // if the enemys health is less then or equal to 0
		if(enemyHealth <=0)
        {
            // spawns in the enemy pasticle effect at the gameobjects position and rotation
            Instantiate(enemyDeathParticle, transform.position, transform.rotation);

            // gameobject is set to inactive
            gameObject.SetActive(false);

            // spawns in a random object from the drop tems array
            Instantiate(dropItems[Random.Range(0, dropItems.Length)], transform.position, Quaternion.identity);
        }
	}
    // this is a function to be used to apply damage to the enemys health
    public void ApplyDamage(int damageGiven)
    {
        // subtracts the enemys health by the damage given
        enemyHealth -= damageGiven;
        
        // sets the rigidbody2d's velocity to 0 on its x-axis and applys the knockback to its y-axis     
        enemyRigidbody.velocity = new Vector2(0, knockbackforceY);

        // plays the audio for the enemys hurt audio source
        hit.Play();
    }
}
//[GAVIN END]