﻿//Student Number: X00109563
//Name: Gavin Hickey
//[GAVIN START]
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
// this script is used for the games main menu which has many different buttons used for various functions within this script
public class MainMenu : MonoBehaviour {

    // variables to set starter player prefs for each gametype, the name of the game types 
    // and login screen
    public string newGame;
    public string InfiniteRunner;
    public int startingLives;
    public int RunnerLives;
    public GameObject login;

    // this functiion switches the scene to the first level and sets the player prefs the the starting values
    public void NewGame()
    {
        SceneManager.LoadScene(newGame);
        PlayerPrefs.SetInt("CoinCount", 0);
        PlayerPrefs.SetInt("PlayerLives", startingLives);
        PlayerPrefs.SetInt("Score", 0);
    }

    // this functiion switches the scene to the infinite runner level 
    // and sets the player prefs of the starting values with the player lives equalling to only one 
    public void InifiniteRunner()
    {
        SceneManager.LoadScene(InfiniteRunner);
        PlayerPrefs.SetInt("CoinCount", 0);
        PlayerPrefs.SetInt("PlayerLives",RunnerLives);
        PlayerPrefs.SetInt("Score", 0);
    }

    // this end the application and exits it
    public void Exit()
    {
        Application.Quit();
    }

    // this opens the login menu
    void ActivateLogin()
    {
        login.SetActive(true);
    }

    //exits the login menu
    void deactivateLogin()
    {
        login.SetActive(false);
    }
}
//[GAVIN END]